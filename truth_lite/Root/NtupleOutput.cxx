#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <truth_lite/NtupleOutput.h>
#include <truth_lite/EventInit.h>
#include <truth_lite/CommonData.h>
#include <truth_lite/Util.h>

#include "AsgTools/IAsgTool.h"


#include <Math/VectorUtil.h>
using ROOT::Math::VectorUtil::Phi_mpi_pi;

using namespace truth_lite;

// this is needed to distribute the algorithm to the workers
ClassImp(NtupleOutput)

NtupleOutput::NtupleOutput()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}

EL::StatusCode NtupleOutput::setupJob(EL::Job& /*job*/)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode NtupleOutput::histInitialize()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode NtupleOutput::fileExecute()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode NtupleOutput::changeInput(bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode NtupleOutput::initialize()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  EventInit *eventInit = dynamic_cast<EventInit*>(wk()->getAlg("EventInit"));
  if (!eventInit) {
    Error("initialize()", "Failed to find EventInit algorithm");
    return EL::StatusCode::FAILURE;
  }
  commonData = eventInit->getCommonData();

  ntupleSvc = EL::getNTupleSvc(wk(), outputName);
  initializeTree(ntupleSvc->tree());

  return EL::StatusCode::SUCCESS;
}

template <typename T, typename Object>
T GetAuxOpt(const Object& obj, const std::string& name, T def)
{
  if (obj->template isAvailable<T>(name))
    return obj->template auxdata<T>(name);
  return def;
}

template <typename Container>
size_t GetContOptSize(const Container* cont)
{
  if (!cont)
    return 0;
  return cont->size();
}

template <typename T, typename Container>
T GetContAuxOpt(const Container* cont, size_t index, const std::string& name, T def) 
{
  if (cont && cont->size() > index) {
    return GetAuxOpt(cont->at(index), name, def);
  }
  else {
    return def;
  }
}

template <typename Container>
bool SetOptPtEtaPhiE(const Container* cont, size_t index, float& pt, float& eta, float& phi, float e)
{
  if (cont && cont->size() > index) {
    auto particle = cont->at(index);

    pt = particle->pt();
    eta = particle->eta();
    phi = particle->phi();
    e = particle->e();
    return true;
  }
  else {
    pt = -999;
    eta = -999;
    phi = -999;
    e = -999;
    return false;
  }
}

EL::StatusCode NtupleOutput::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  auto& cd = commonData;

  for (const auto& co: containers) {
    FillParticleArray(co.first);
  }

  for (auto& met: metOutputs) {
    FillMet(met.second);
  }

  auto& eventInfo = cd->eventInfo;


  for (auto& dynPair: dynamicDoubles) {
    dynPair.second = GetAuxOpt<double>(eventInfo, dynPair.first, 0.);
  }

  for (auto& dynPair: dynamicFloats) {
    dynPair.second = GetAuxOpt<float>(eventInfo, dynPair.first, 0.);
  }

  for (auto& dynPair: dynamicInts) {
    dynPair.second = GetAuxOpt<int>(eventInfo, dynPair.first, 0);
  }

  for (auto& dynPair: dynamicUInts) {
    dynPair.second = GetAuxOpt<uint32_t>(eventInfo, dynPair.first, 0);
  }

  for (auto& dynPair: dynamicULongs) {
    dynPair.second = GetAuxOpt<unsigned long long>(eventInfo, dynPair.first, 0);
  }

  ntupleSvc->tree()->Fill();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode NtupleOutput::postExecute()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode NtupleOutput::finalize()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode NtupleOutput::histFinalize()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}

template <typename T>
void NtupleOutput::AddDynamicBranches(TTree *tree, const std::vector<std::pair<std::string,std::string>>& names, std::vector<std::pair<std::string, T>>& storage, const char *typeDef)
{
  storage.reserve(names.size());

  // First create the vector...
  for (const auto& auxname_outname: names) {
    storage.push_back(std::make_pair(auxname_outname.first, T()));
  }

  // ...then assign the branches. This is important, as the
  // floats must not be moved after giving their addresses
  // to tree->Branch(), otherwise "bad things" (TM) will happen!
  for (size_t i=0; i < names.size(); ++i) {
    const auto& outname = names[i].second;
    AddBranch(tree, outname, &storage[i].second, outname + typeDef);
  }
}

void NtupleOutput::initializeTree(TTree *tree)
{
  for (const auto& entry: commonData->outputParticles) {
    AddParticleArray(tree, entry);
  }

  for (const auto& entry: commonData->outputMet) {
    AddMet(tree, entry);
  }

  AddDynamicBranches(tree, commonData->outputDoubles, dynamicDoubles, "/F");
  AddDynamicBranches(tree, commonData->outputFloats, dynamicFloats, "/F");
  AddDynamicBranches(tree, commonData->outputInts, dynamicInts, "/I");
  AddDynamicBranches(tree, commonData->outputUInts, dynamicUInts, "/I");
  AddDynamicBranches(tree, commonData->outputULongs, dynamicULongs, "/I");
}

bool NtupleOutput::BranchFilterAccept(const std::string& /*name*/)
{
  // TODO: add filter method
  return true;
}

void NtupleOutput::AddBranch(TTree *tree, const std::string& name, void *ptr, const std::string& type, const std::string& desc)
{
  if (BranchFilterAccept(name)) {
    TBranch *br = tree->Branch(name.c_str(), ptr, type.c_str());
    if (!desc.empty()) {
      br->SetTitle(desc.c_str());
    }
  }
}

static std::string ArrayType(const std::string& name, const std::string& len, const std::string& type)
{
  return name + "[" + len + "]/" + type;
}

void NtupleOutput::AddParticleArray(TTree *tree, const Output::Particle& def)
{
  auto& co = containers[def.containerName];

  auto len = "n_" + def.prefix;

  AddBranch(tree, len, &co.length, len+"/I");
  AddBranch(tree, def.prefix+"_pt", co.pt.data(), ArrayType(def.prefix+"_pt", len, "F"));
  AddBranch(tree, def.prefix+"_eta", co.eta.data(), ArrayType(def.prefix+"_eta", len, "F"));
  AddBranch(tree, def.prefix+"_phi", co.phi.data(), ArrayType(def.prefix+"_phi", len, "F"));
  AddBranch(tree, def.prefix+"_e", co.e.data(), ArrayType(def.prefix+"_e", len, "F"));

  for (const auto& deco: def.decorations) {
    const std::string auxvar = deco.auxvar;
    const std::string outname = deco.outname;
    const char type = deco.type;

    switch (type) {
    case 'F':
      AddBranch(tree, def.prefix + "_" + outname, co.decorationsF[auxvar].data(), 
		ArrayType(def.prefix + "_" + outname, len, "F"));
      break;
    case 'D':
      AddBranch(tree, def.prefix + "_" + outname, co.decorationsD[auxvar].data(), 
		ArrayType(def.prefix + "_" + outname, len, "D"));
      break;
    case 'C':
      AddBranch(tree, def.prefix + "_" + outname, co.decorationsC[auxvar].data(), 
		ArrayType(def.prefix + "_" + outname, len, "C"));
      break;
    case 'I':
      AddBranch(tree, def.prefix + "_" + outname, co.decorationsI[auxvar].data(), 
		ArrayType(def.prefix + "_" + outname, len, "I"));
      break;
    case 'L':
      AddBranch(tree, def.prefix + "_" + outname, co.decorationsL[auxvar].data(), 
		ArrayType(def.prefix + "_" + outname, len, "L"));
      break;
    default:
      break;
    }

  }
}

void NtupleOutput::FillParticleArray(const std::string& containerName)
{
  auto& co = containers[containerName];

  co.length = 0;
  auto store = wk()->xaodStore();

  if (!store->contains<xAOD::IParticleContainer>(containerName))
    return;

  const auto& particles = Retrieve<xAOD::IParticleContainer>(store, containerName);
  co.length = static_cast<int>(particles->size());

  if (co.length > static_cast<int>(co.MAX_LEN)) {    
    Warning("FillParticleArray()", "hit limit on number of particles for output TTree, containerName: '%s'.", containerName.c_str() );
    Warning("FillParticleArray()", "Limit is set to %i, but this event has %i entries.", static_cast<int>(co.MAX_LEN), static_cast<int>(co.length));
    Warning("FillParticleArray()", "Will drop everything beyond limit (in output TTree).");
    co.length = co.MAX_LEN;
  }

  for (size_t i=0; i < std::min(co.MAX_LEN, particles->size()); ++i) {
    const auto& p = (*particles)[i];
    co.pt[i] = p->pt();
    co.eta[i] = p->eta();
    co.phi[i] = p->phi();
    co.e[i] = p->e();

    for (auto& deco: co.decorationsD) {
	deco.second[i] = p->auxdata<double>(deco.first);
    }
    for (auto& deco: co.decorationsF) {
	deco.second[i] = p->auxdata<float>(deco.first);
    }
    for (auto& deco: co.decorationsC) {
	deco.second[i] = p->auxdata<char>(deco.first);
    }
    for (auto& deco: co.decorationsI) {
	deco.second[i] = p->auxdata<int>(deco.first);
    }
    for (auto& deco: co.decorationsL) {
	deco.second[i] = p->auxdata<long>(deco.first);
    }

  }
}

void NtupleOutput::AddMet(TTree *tree, const Output::Met& def)
{
  auto& co = metOutputs[def.prefix];

  co.container = def.container;
  co.name = def.name;

  AddBranch(tree, def.prefix, &co.met, def.prefix + "/F");
  if (!def.onlyMet) {
    AddBranch(tree, def.prefix + "_x", &co.met_x, def.prefix + "_x/F");
    AddBranch(tree, def.prefix + "_y", &co.met_y, def.prefix + "_y/F");
    AddBranch(tree, def.prefix + "_phi", &co.met_phi, def.prefix + "_phi/F");
    AddBranch(tree, def.prefix + "_sumet", &co.met_sumet, def.prefix + "_sumet/F");
  }
}

void NtupleOutput::FillMet(MetOutput& out)
{
  auto store = wk()->xaodStore();
  const auto& metContainer = Retrieve<xAOD::MissingETContainer>(store, out.container);

  if (metContainer->find(out.name) == metContainer->end()) {
    Error("FillMet()", "Failed to find MET component '%s' in container '%s'", out.name.c_str(), out.container.c_str());

    Error("FillMet()", "Available components:");
    for (const auto& cmp: *metContainer) {
      Error("FillMet()", "  %s", cmp->name().c_str());
    }
  }

  const auto& met = *metContainer->find(out.name);

  out.met = met->met();
  out.met_x = met->mpx();
  out.met_y = met->mpy();
  out.met_phi = met->phi();
  out.met_sumet = met->sumet();
}
