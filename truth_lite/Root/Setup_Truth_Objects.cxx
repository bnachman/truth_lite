#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <truth_lite/Setup_Truth_Objects.h>
#include <truth_lite/EventInit.h>
#include <truth_lite/CommonData.h>
#include <truth_lite/Util.h>
#include <xAODBase/IParticleHelpers.h>

#include <xAODMuon/MuonAuxContainer.h>
#include <xAODEgamma/ElectronAuxContainer.h>
#include <xAODMissingET/MissingETAuxContainer.h>

using namespace truth_lite;

// this is needed to distribute the algorithm to the workers
ClassImp(Setup_Truth_Objects)

Setup_Truth_Objects::Setup_Truth_Objects()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}

EL::StatusCode Setup_Truth_Objects::setupJob(EL::Job& /*job*/)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Setup_Truth_Objects::histInitialize()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Setup_Truth_Objects::fileExecute()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Setup_Truth_Objects::changeInput(bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Setup_Truth_Objects::initialize()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  EventInit *eventInit = dynamic_cast<EventInit*>(wk()->getAlg("EventInit"));
  if (!eventInit) {
    Error("initialize()", "Failed to find EventInit algorithm");
    return EL::StatusCode::FAILURE;
  }
  commonData = eventInit->getCommonData();

  commonData->outputParticles.push_back({
      "SignalJets",
        "jet",
          {
           {"Jvt","Jvt"},
           {"btagged", "btagged",'I'},
          }
    });

  commonData->outputParticles.push_back({
      "SignalElectrons",
        "electrons",
          {
            {"etcone20", "etcone20"},
            {"ptcone30", "ptcone30"},
          }
    });
  
  commonData->outputParticles.push_back({
      "SignalMuons",
        "muons",
          {
            {"etcone20", "etcone20"},
            {"ptcone30", "ptcone30"},
          }
    });
  
  commonData->outputMet.push_back({
      "MET", "Final",
        "met",
        false,
        });

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Setup_Truth_Objects::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  
  xAOD::TEvent *event = wk()->xaodEvent();
  xAOD::TStore *store = wk()->xaodStore();

  auto cd = commonData;

  /*//////////////
    First, the MET
  */////////////
  
  auto my_truth_met = Retrieve<xAOD::MissingETContainer>(event, "MET_Truth");
  cd->met = MakeContainer<xAOD::MissingETContainer, xAOD::MissingETAuxContainer>();
  auto& met = cd->met;
  xAOD::MissingET* metFinal = new xAOD::MissingET(0.,0.,0.,"Final",MissingETBase::Source::total());
  met.first->push_back(metFinal);
  //There are several truth met 'flavors'.  The first one (->at(0)) is constructed from all non-interacting particles (neutrinos, neutralinos, etc.)
  met.first->back()->setMpx(my_truth_met->at(0)->mpx());
  met.first->back()->setMpy(my_truth_met->at(0)->mpy());
  met.first->back()->setSumet(my_truth_met->at(0)->sumet());
  Record(store, met, "MET");
  
  CHECK_THROW(met.first->find("Final") != met.first->end());
  commonData->metRefFinal = *met.first->find("Final");

  /*//////////////
    Next, the jets
  */////////////
  
  cd->jets = Retrieve<xAOD::JetContainer>(event, "AntiKt4TruthJets");
  auto& jets = cd->jets;
  cd->calibJets = SaveShallowCopy(*jets);
  auto& calibJets = cd->calibJets;

  //Signal jet definition:
  double signalJetPt = 25*1000;
  double signalJetEta = 2.5;

  for (const auto& jet: *calibJets.first) {
    jet->auxdata<float>("Jvt") = 1.0; //Always hard-scatter !  It is possible to add also pile-jets, but the TRUTHx derivations will not have them.
    jet->auxdata<int>("btagged") = jet->auxdata<int>("GhostBHadronsFinalCount");
    bool isSignal =  ((jet->pt() > signalJetPt) && (fabs(jet->eta()) < signalJetEta));
    dec_signal(*jet) = isSignal; //dec_signal is a defined in truth_lite/Util.h
  }
  CHECK_THROW(xAOD::setOriginalObjectLink(*jets, *calibJets.first));

  cd->signalJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
  auto& signalJets = cd->signalJets;
  CHECK_THROW(store->record(signalJets, "SignalJets"));

  CopyViewIf<xAOD::Jet>(calibJets, signalJets, [this](const xAOD::Jet& jet) {
      return (dec_signal(jet)); //only save the jets that have passed the 'Signal jet' definition.
    });
  std::sort(signalJets->begin(), signalJets->end(), ComparePt);

  /*//////////////
    Now for muons.
  */////////////
  
  cd->tmuons = Retrieve<xAOD::TruthParticleContainer>(event, "TruthMuons");
  auto& muons = cd->tmuons;
  cd->calibtMuons = SaveShallowCopy(*muons);
  auto& calibMuons = cd->calibtMuons;
  
  for (const auto& muon: *calibMuons.first) {
    dec_signal(*muon) = (muon->auxdata<unsigned int>("classifierParticleOrigin")==12); //let's save all the muons from W bosons.
  } 
  CHECK_THROW(xAOD::setOriginalObjectLink(*muons, *calibMuons.first));
  
  //Now, the muons above are xAOD:TruthParticles - to make them more compatible with reco analysis code, let's save them as xAOD:Muons
  cd->signalMuons = new xAOD::MuonContainer();
  auto& signalMuons = cd->signalMuons;
  auto signalMuonsAux = new xAOD::MuonAuxContainer();
  signalMuons->setStore(signalMuonsAux);
  auto SignalMuonsPair = std::make_pair(signalMuons, signalMuonsAux);
  Record(store, SignalMuonsPair, "SignalMuons");

  for (const auto& muon: *calibMuons.first){
    if (!(int)dec_signal(*muon)) continue;
    signalMuons->push_back(new xAOD::Muon());
    signalMuons->back()->auxdata<float>("z0") = 1.;
    signalMuons->back()->setP4(muon->p4().Pt(),muon->p4().Eta(),muon->p4().Phi());
  }
  std::sort(signalMuons->begin(), signalMuons->end(), ComparePt);

  /*//////////////
    Now for electrons.
  */////////////
  
  cd->telectrons = Retrieve<xAOD::TruthParticleContainer>(event, "TruthElectrons");
  auto& electrons = cd->telectrons;
  cd->calibtElectrons = SaveShallowCopy(*electrons);
  auto& calibElectrons = cd->calibtElectrons;

  for (const auto& el: *calibElectrons.first) {
    dec_signal(*el) = (el->auxdata<unsigned int>("classifierParticleOrigin")==12); //let's save all the muons from W bosons.
  }
  CHECK_THROW(xAOD::setOriginalObjectLink(*electrons, *calibElectrons.first));

  cd->signalElectrons = new xAOD::ElectronContainer();
  auto& signalElectrons = cd->signalElectrons;
  auto signalElectronsAux = new xAOD::ElectronAuxContainer();
  signalElectrons->setStore(signalElectronsAux);
  auto signalElectronsPair = std::make_pair(signalElectrons, signalElectronsAux);
  Record(store, signalElectronsPair, "SignalElectrons");

  for (const auto& el: *calibElectrons.first){
    if (!(int)dec_signal(*el)) continue;
    signalElectrons->push_back(new xAOD::Electron());
    signalElectrons->back()->auxdata<float>("ptcone30") = el->auxdata<float>("ptcone30");
    signalElectrons->back()->auxdata<float>("etcone20") = el->auxdata<float>("etcone20");
    signalElectrons->back()->setP4(el->p4().Pt(),el->p4().Eta(),el->p4().Phi(),el->p4().M());
  }
  std::sort(signalElectrons->begin(), signalElectrons->end(), ComparePt);
  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Setup_Truth_Objects::postExecute()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Setup_Truth_Objects::finalize()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Setup_Truth_Objects::histFinalize()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
