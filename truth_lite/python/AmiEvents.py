AMI_EVENTS = {}

# powheg_ttbar_50ns
AMI_EVENTS.update({407012: 3954981, 410000: 19954442})

# sherpa_Wjets_50ns
AMI_EVENTS.update({361324: 19771553, 361362: 9940})

# madgraph_ttZ_50ns
AMI_EVENTS.update({410073: 1999400})
