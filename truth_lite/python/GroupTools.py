"""
truth_lite.GroupTools

Tools to access sample groups as if they are samples. Groups are collections of samples
or sub-samples, which behave as if they are a single sample.
"""
import sys
import os
from glob import glob

from ROOT import *
import truth_lite.SampleTools as Smp
from truth_lite.SampleTools import Sample

COMMON_GROUP_FILE = "truth_lite/share/sample_groups.common.py"
USER_GROUP_FILE = "truth_lite/share/sample_groups.local.py"

Groups = {}

def _load_file(path):
	env = {}
	execfile(path, env)

	if "SampleGroups" not in env or type(env['SampleGroups']) != dict:
		print "Error: Invalid group definition in '%s'" % path
		print "       No dictionary 'SampleGroups' found."
		return

	file_groups = env['SampleGroups']

	for group, group_def in file_groups.iteritems():
		if group not in Groups:
			Groups[group] = group_def

def _init():
	"""
	Initialise the group definitions from both the user configuration
	and the common configuration

	This function is automatically called when the module is included.
	"""
	workDir = os.getenv("WorkDir")

	user_file = os.path.join(workDir, USER_GROUP_FILE)
	if os.path.exists(user_file):
		_load_file(user_file)

	common_file = os.path.join(workDir, COMMON_GROUP_FILE)
	if os.path.exists(common_file):
		_load_file(common_file)

def _resolve_samples(pattern):
	#from array import array

	workDir = os.getenv("WorkDir")
	#print 'debug pattern=',pattern
	results = map(os.path.basename, glob(os.path.join(workDir, "samples", pattern)))
	#all_samples = map(os.path.basename, glob(os.path.join(workDir, "samples", "*")))
	#import re
	#exp = re.compile(pattern)
	#results = filter( exp.search, all_samples)
	# following code doesn't work for me (Till)
	#exp = TRegexp(pattern)
	#results = []
	#for sample in all_samples:
	#	length = array('i', [0]) # we need a ptr to an int here, for python fake it with an array of 1 element
	#	idx = exp.Index(sample, length)
	#	if idx == 0 and length[0] == len(sample):
	#		results.append(sample)

	return results

def _do_get_group_samples(group_def):
	samples = []

	for entry in group_def:
		sample_pattern = entry
		pattern = ".*"
		if "#" in entry:
			sample_pattern, pattern = entry.split("#", 1)

		matching_samples = _resolve_samples(sample_pattern)
		samples.extend(matching_samples)

	# return a list of unique samples
	return list(set(samples))

def _load_group_sh(gid, group_def, data_type):
	sh = SH.SampleHandler()

	for entry in group_def:
		sample_pattern = entry

		if "#" in entry:
			sample_pattern, _ = entry.split("#", 1)

		matching_samples = _resolve_samples(sample_pattern)

		for sample_name in matching_samples:
			smp_path = Smp.output_path(sample_name, gid.config, gid.syst)

			sample_sh = SH.SampleHandler()
			sample_sh.load(os.path.join(smp_path, data_type))

			sh.add(sample_sh.findByName(pattern))

	return sh

def _do_load_group(group_id, data_type):
	group_def = Groups[group_id.name]

	sh_hist = _load_group_sh(group_id, group_def, "hist")

	if data_type == "hist":
		return Smp.Sample(
			data=None, hist=sh_hist,
			name=group_id.name, config=group_id.config,
			syst=group_id.syst, title=group_id.title,
		)

	sh_data = _load_group_sh(group_id, group_def, "output-" + data_type)
	sh_data.setMetaString("nc_tree", "CollectionTree")

	return Sample(
		data=sh_data, hist=sh_hist,
		name=group_id.name, config=group_id.config,
		syst=group_id.syst, title=group_id.title,
	)


def get_group_samples(name):
	if name not in Groups:
		return []

	return _do_get_group_samples(Groups[name])

def load_group(opts, name, data_type="hist"):
	"""
	Load a group from the typical sample definition

	The resulting object can be used as if it was a normal sample,
	everything should work as before.
	"""
	sample_id = Smp.parse_sample_id(opts, name)

	if sample_id.name not in Groups:
		return Smp.load_output_sample_from_id(sample_id, data_type)

	return _do_load_group(sample_id, data_type)

def load_all_groups(opts, data_type="hist", field="samples"):
	"""
	Parse and load all groups or samples that are specified
	in opts[field], and replace that entry
	"""
	result = []

	for smp in getattr(opts, field):
		obj = load_group(opts, smp, data_type)
		result.append(obj)

	setattr(opts, "orig_" + field, getattr(opts, field))
	setattr(opts, field, result)

_init()
