import sys
import os

from ROOT import *

from collections import namedtuple

import truth_lite.SampleTools as Smp

OutputTypes = [
	(".png", None),
	(".pdf", "pdf/"),
	(".root", "root/"),
]

def add_bins(hist, target, bin2):
	from math import sqrt
	v0 = hist.GetBinContent(target)
	e0 = hist.GetBinError(target)

	v1 = hist.GetBinContent(bin2)
	e1 = hist.GetBinError(bin2)

	hist.SetBinContent(target, v0+v1)
	hist.SetBinError(target, sqrt(e0**2 + e1**2))

	hist.SetBinContent(bin2, 0)
	hist.SetBinError(bin2, 0)

def draw_hist_from_tree(tree, var_name, cut_weight, hist_name=None, binning=None, overflow=True):
	if "ReweightVLQ" in cut_weight:
		gROOT.LoadMacro("truth_lite/share/ReweightVLQ.cxx+")

	if not hist_name:
		hist_name = tree.GetName() + "_" + var_name

	draw_cmd = "{var}>>{hist}".format(var=var_name, hist=hist_name)
	if binning:
		draw_cmd += ("(%s)" % binning)

	n_entries = tree.Draw(draw_cmd, cut_weight, "e")
	hist = gDirectory.Get(hist_name)
	if not hist:
		return None

	hist.SetDirectory(gROOT)

	if overflow:
		add_bins(hist, hist.GetNbinsX(), hist.GetNbinsX()+1)

	return hist

def draw_hist(sample, var_name, cut_weight, hist_name=None, binning=None, overflow=True, weighted=True):
	hist = None

	for i in xrange(sample.hist.size()):
		tree = sample.data.at(i).makeTChain()

		# skip empty sub-samples
		# this can happen for small data runs
		if tree.GetEntries() == 0:
			continue

		sub_name = hist_name
		if i > 0:
			sub_name += str(i)
		sub_hist = draw_hist_from_tree(tree, var_name, cut_weight, sub_name, binning, overflow)
		if not sub_hist:
			print "Failed to load histogram from subsample", sample.data.at(i).name()
			return None

		if weighted:
			wgt = Smp.sample_weight(sample.hist.at(i))
			sub_hist.Scale(wgt)

		if not hist:
			hist = sub_hist.Clone()
		else:
			hist.Add(sub_hist)

		# Explicitely delete the tree, in order to keep the number
		# of open files down.
		tree.Delete()
		del tree

	return hist

def normalize(hist):
	integral = hist.Integral()
	if integral > 0:
		hist.Scale(1.0 / integral)
	return hist

def set_titles(hist, xtitle, ytitle, xunit=None, binning=False):
	if xunit and xunit != "":
		hist.GetXaxis().SetTitle("%s [%s]" % (xtitle, xunit))
	else:
		hist.GetXaxis().SetTitle(xtitle)

	if type(binning) == bool and not binning:
		hist.GetYaxis().SetTitle(ytitle)
	else:
		bw = hist.GetBinWidth(1)
		if type(binning) == int:
			bin_width = "%g" % (round(bw, binning))
		elif type(binning) == str:
			bin_width = binning % bw
		else:
			bin_width = str(bw)

		if not xunit:
			xunit = ""

		hist.GetYaxis().SetTitle("%s / %s %s" % (ytitle, bin_width, xunit))

def set_titles_2d(hist, xtitle, ytitle, ztitle, xunit=None, yunit=None):
	if xunit and xunit != "":
		hist.GetXaxis().SetTitle("%s [%s]" % (xtitle, xunit))
	else:
		hist.GetXaxis().SetTitle(xtitle)

	if yunit and yunit != "":
		hist.GetYaxis().SetTitle("%s [%s]" % (ytitle, yunit))
	else:
		hist.GetYaxis().SetTitle(ytitle)

	hist.GetZaxis().SetTitle(ztitle)

def set_bin_labels(hist, labels):
	xa = hist.GetXaxis()
	for bin in xrange(1, hist.GetNbinsX()+1):
		xa.SetBinLabel(bin, labels[bin-1])

def qadd(hists):
	from math import sqrt

	hres = hists[0].Clone()

	for bin in xrange(0, hres.GetNbinsX()+2):
		val2 = 0
		for h in hists:
			val2 += h.GetBinContent(bin)**2
		hres.SetBinContent(bin, sqrt(val2))

	return hres

def check_output_path(base_path):
	for ext, sub_path in OutputTypes:
		p = base_path
		if sub_path:
			p = os.path.join(p, sub_path)

		if not os.path.exists(p):
			os.makedirs(p)

def save_canv(canv, base_path, name=None):
	if not name:
		name = canv.GetName()

	check_output_path(base_path)

	for ext, sub_path in OutputTypes:
		p = base_path
		if sub_path:
			p = os.path.join(p, sub_path)
		p = os.path.join(p, name + ext)

		canv.SaveAs(p)

def _load_cut_def(cut_def):
	workDir = os.getenv("WorkDir")
	file_name = os.path.join(workDir, "truth_lite/share/cuts/", cut_def + ".py")

	if not os.path.exists(file_name):
		print "Failed to load cut definitions, file does not exist: '%s'" % file_name
		return None

	_cutType = namedtuple("Cut", "id title cut_func")

	cuts = {}

	def _dummy(*args, **kwargs):
		pass

	def _add_vc_ex(name, cut_list, save_sub_results=False):
		if save_sub_results:
			cuts[name] = cut_list

	interface = {
		'add_virtual_cut': _dummy,
		'add_virtual_cut_ex': _add_vc_ex,
		'Cut': _cutType,
	}

	execfile(file_name, interface)

	return cuts

def get_n1_cut(cut_def, cut_name, sub_name):
	cuts = _load_cut_def(cut_def)
	if not cuts:
		return None

	if not cut_name in cuts:
		print "Error: Failed to find cut '%s' in cut definition '%s'" % (cut_name, cut_def)
		return None

	cut_list = cuts[cut_name]

	found_subcut = False
	for entry in cut_list:
		if sub_name == entry.id:
			found_subcut = True
			break
	if not found_subcut:
		print "Error: Cut '%s' does not contain '%s'" % (cut_name, sub_name)
		return None

	n1_cuts = [
		"%s_%s" % (cut_name, entry.id) for entry in cut_list if entry.id != sub_name
	]

	return "(%s)" % (" * ".join(n1_cuts))

_pu_var_map = {
	'mu': 'av_int_per_xing',
	'pv': 'num_pv',
}

_pu_steps = [
	'AfterPresel'
]

_pu_hists = {
	'mu': "AverageInt",
	'pv': "NumVertices",
}

_channel_cuts = {
	'el': "n_el > 0",
	'mu': "n_mu > 0",
}

def add_common_options(parser, rename_cut=False, single_var=True):
	"""
	Add common options to the ArgumentParser object.
	"""
	workDir = os.getenv("WorkDir")
	def_plot_dir = os.path.join(workDir, "plots")

	parser.add_argument("-o", "--output", default=def_plot_dir, help="Output path")
	parser.add_argument("-n", "--name")
	parser.add_argument("-b", "--binning", help="Binning")
	parser.add_argument("--log", action="store_true")
	if rename_cut:
		parser.add_argument("--presel", help="Cut to apply")
	else:
		parser.add_argument("--cut", help="Cut to apply")
	parser.add_argument("--n1", help="N-1 cut to apply, format: cutdef,cutname,subcut")

	parser.add_argument("--channel", default="both", choices=["both"] + _channel_cuts.keys(), help="Lepton channel, default: both")

	parser.add_argument("--pu", action="store_true", help="Enable pileup reweighting")
	parser.add_argument("--pu-var", default="mu", choices=_pu_var_map.keys(), help="Pileup reweighting reference variable")
	parser.add_argument("--pu-step", default="AfterPresel", choices=("AfterPresel"), help="Use PU histograms for this step")
	parser.add_argument("--pu-ref", help="Pileup reweighting reference sample")

	if single_var:
		parser.add_argument("var", help="The variable to plot")
	else:
		parser.add_argument("-v", "--var", action="append", dest="variables", help="Variable to plot")

def sanitize_var(var):
	for func in ('abs', 'log', 'exp'):
		if func + "(" in var:
			var = var.replace(func, func + "_")

	if "/" in var:
		var = var.replace("/", "_by_")

	for x in ('(', ')', '[', ']'):
		var = var.replace(x, "")

	return var

def get_pu_cutweight(opts):
	if opts.pu:
		ref_var = _pu_var_map[opts.pu_var]
		return "ReweightSF(%s)" % ref_var
	return None

def process_common_options(opts):
	"""
	Process the common options added by 'add_common_options' directly in the resulting opts

	This function adds a opts.cut_weight field, that contains the string that should be used
	as the cut_weight definition for the plot.
	In addition the opts.name field will be populated with meaniningfull content.
	"""
	if not opts.name:
		opts.name = sanitize_var(opts.var)

	if "cut" not in opts:
		return

	if opts.cut and opts.n1:
		print "Warning: Both --cut and --n1 specified. Are you sure you want to do that?"

	if opts.n1:
		parts = opts.n1.split(",")
		if len(parts) != 3:
			print "Error: --n1 expects cutdef,cutname,subcut"
			sys.exit(1)

		opts.n1_cut = get_n1_cut(parts[0], parts[1], parts[2])
		if not opts.n1_cut:
			sys.exit(1)
	else:
		opts.n1_cut = None

	opts.cut_weight = "weight"

	if opts.cut:
		opts.cut_weight += " * (%s)" % opts.cut
	if opts.n1_cut:
		opts.cut_weight += " * (%s)" % opts.n1_cut

	if opts.channel != "both":
		opts.cut_weight += " * (%s)" % _channel_cuts[opts.channel]

	opts.cut_weight_data = opts.cut_weight

	if opts.pu:
		ref_var = _pu_var_map[opts.pu_var]
		opts.cut_weight += " * ReweightSF(%s)" % ref_var
		# do not add to cut_weight_data !!!


def setup_pileup_reweighting(opts, sample, ref_sample, output_sf_hist=False):
	pu_hist = _pu_hists[opts.pu_var]

	h_smp = Smp.read_hist(sample, "%s_%s" % (pu_hist, opts.pu_step))
	if not h_smp:
		print "Failed to read pileup reweighting histogram for ", Smp.sample_name(sample)
		sys.exit(1)

	h_ref = Smp.read_hist(ref_sample, "%s_%s" % (pu_hist, opts.pu_step))
	if not h_smp:
		print "Failed to read pileup reweighting histogram for ", Smp.sample_name(ref_sample)
		sys.exit(1)

	h_smp.Scale(1. / h_smp.Integral())
	h_ref.Scale(1. / h_ref.Integral())

	h_sf = h_ref.Clone()
	h_sf.Divide(h_smp)
	h_sf.SetName("sf")

	if output_sf_hist:
		h_sf.SaveAs(output_sf_hist)

	gROOT.LoadMacro("truth_lite/share/ReweightFunc.cxx+")
	gROOT.ProcessLine("g_SFhist = (TH1F*)sf->Clone();")

def cleanup_pileup_reweighting(opts):
	gROOT.ProcessLine("delete g_SFhist; g_SFhist = 0;")
