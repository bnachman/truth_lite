"""
Luminosity information from iLumiCalc.exe
"""
import os
import csv

DEFAULT_FILE="lumitable-data15_13TeV.periodAllYear_DetStatus-v64-pro19_DQDefects-00-01-02_PHYS_StandardGRL_All_Good.csv"

_db = {}

def get_run_lumi(run, field="Prescale Corrected", raw=False):
	"""
	Return the luminosity of a run (or "Total") in 1/pb
	"""
	if type(run) == int:
		run = str(run)

	if not run in _db:
		print "Error: Run %s not found in luminosity database. It is probably not in the GRL, either too new or completely bad." % run
		return -1

	entry = _db[run]
	if field not in entry:
		print "Error: Access to invalid luminosity type '%s'" % field
		return -1

	if raw:
		return entry[field]

	if "Fraction" in field:
		return float(entry[field])
	elif field in ["Run", "Good", "Bad"]:
		return int(entry[field])
	else:
		return float(entry[field]) / 1000.

def _init_db(reader):
	fields = reader.next()

	for row in reader:
		run = row[0]

		entry = {}
		for i, field in enumerate(fields):
			entry[field] = row[i]

		_db[run] = entry

def init(file_name=DEFAULT_FILE):
	workDir = os.getenv("WorkDir")
	infile = open(os.path.join(workDir, "truth_lite", "share", file_name), "r")
	reader = csv.reader(infile, skipinitialspace=True)

	_init_db(reader)

