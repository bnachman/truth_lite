# This file defines all functions to setup and submit jobs. It
# is used by the run.py and submit.py scripts.
import sys
import os
import pickle
from ROOT import *

import truth_lite.BatchTools as BT
import truth_lite.SampleTools as ST

if not 'SH' in globals() and '_ARGCOMPLETE' not in os.environ:
	print "Error: RootCore has to be initialised before importing RunTools!"
	sys.exit(1)

def _enum(**enums):
    return type('Enum', (), enums)

Mode = _enum(local="LOCAL", batch="BATCH", grid="GRID")

def load_config(opts):
	glob = globals()
	glob["CmdLineOptions"] = opts

	def _inc_cfg(src):
		path = os.path.join(opts.workDir, "truth_lite/share/config", src + ".py")
		execfile(path, glob)

	def _exec_extra():
		if opts.exec_lines and len(opts.exec_lines) > 0:
			print "WARNING: %d extra lines of configuration are executed" % len(opts.exec_lines)
			print "WARNING: this is meant _only_ for very small tests,"
			print "WARNING: please consider changing the configuration instead."
					
			for line in opts.exec_lines:
				code = compile(line, 'cmdline', 'exec')
				eval(code)

	glob["include_config"] = _inc_cfg
	glob["execute_extra_args"] = _exec_extra
	execfile(opts.config_path, glob)

	if type(Algorithms) != list:
		print "Error: Invalid configuration, 'Algorithms' missing."
		sys.exit(1)

	if type(Output) != list:
		print "Error: Invalid configuration, 'Output' missing."
		sys.exit(1)

	return Algorithms, Output

def load_sample(opts):
	sh = SH.SampleHandler()
	sh.load(opts.sample_path)

	sh.setMetaString("nc_tree", "CollectionTree")

	if opts.systematic:
		sh.setMetaString("systematic", opts.systematic)

	return sh

def create_job(opts, sh):
	job = EL.Job()
	job.sampleHandler(sh)

	job.options().setDouble(EL.Job.optCacheSize, 10*1024*1024)
	job.options().setDouble(EL.Job.optRemoveSubmitDir, 1)

	if opts.nevents:
		job.options().setDouble(EL.Job.optMaxEvents, opts.nevents)

	for out in Output:
		job.outputAdd(out)

	alg_counter = 0
	for alg in Algorithms:
		# give all algorithms a name
		# this is needed in list-systematics.py, as it
		# is the only way to specify an algorithm to retrieve
		# it from the worker		
		if alg.GetName() == "":
			alg.SetName("Alg_" + alg.IsA().GetName() + "_" + str(alg_counter))
			alg_counter += 1
		job.algsAdd(alg)

	return job

def run_job(opts, job):
	driver = EL.DirectDriver()
	driver.submit(job, opts.output)

def batch_submit_job(opts, job):
	job.options().setDouble(EL.Job.optFilesPerWorker, opts.files_per_job)

	job_name = "%s_%s" % (opts.config, opts.sample)
	job.options().setString("nc_jobName", job_name)
	job.options().setString("nc_jobName", job_name)
	if opts.dry_run: job.options().setBool("dry_run", True)
	if opts.verbose: job.options().setBool("verbose", True)

	if not opts.batch:
		bs = BT.detect()
	else:
		bs = opts.batch

	if bs == 'lxbatch':
		job.options().setString(EL.Job.optSubmitFlags, " -L /bin/bash -q %s" % opts.queue)
		driver = EL.LXBatchDriver()
		driver.shellInit = "source {wd}/truth_lite/scripts/batch_setup.sh {wd}".format(wd=opts.workDir)
	elif bs == 'wtal':
		full_out_dir = os.path.join(opts.workDir, opts.output)
		submitOpts = "-e {output} -o {output}".format(output=full_out_dir)
		job.options().setString(EL.Job.optSubmitFlags, submitOpts)
		driver = EL.GEDriver()
		driver.shellInit = "source {wd}/truth_lite/scripts/batch_setup.sh {wd}".format(wd=opts.workDir)
	elif bs == 'gva':
		print "we are running in Geneva!"
 		full_out_dir = os.path.join(opts.workDir, opts.output)
 		submitOpts = " -l vmem=3500mb "
 		job.options().setString(EL.Job.optSubmitFlags, submitOpts+" -q %s" % opts.queue)
 		driver = EL.TorqueDriver()
 		#driver.shellInit = "print 'check work directory: ' + {wd} && source {wd}/truth_lite/scripts/batch_setup.sh {wd} && source {wd}/setenvUNIGE.sh".format(wd=opts.workDir)
		driver.shellInit = "echo 'check work directory: ' {wd} && source {wd}/truth_lite/scripts/batch_setup.sh {wd} && source {wd}/setenvUNIGE.sh".format(wd=opts.workDir)
	elif bs == 'slac':
		job.options().setString(EL.Job.optSubmitFlags, "-q %s -o /nfs/slac/g/atlas/u01/users/bnachman/xAOD/stop1l-xaod/output.log -e /nfs/slac/g/atlas/u01/users/bnachman/xAOD/stop1l-xaod/error.log" % opts.queue)
		driver = EL.LSFDriver()
		driver.shellInit = "source {wd}/truth_lite/scripts/batch_setup.sh {wd}".format(wd=opts.workDir)
		pass

	driver.submitOnly(job, opts.output)
	

def auto_grid_suffix():
	from datetime import datetime

	now_date = datetime.now().date()
	suffix = now_date.isoformat().replace("-", "_")
	
	return suffix

def _check_proxy():
	os.system("voms-proxy-info -all &> /dev/null || voms-proxy-init -voms atlas")

def grid_submit_job(opts, job, fixed_output=None):
	driver = EL.PrunDriver()

	if opts.grid_suffix == "AUTO":
		opts.grid_suffix = auto_grid_suffix()

	if fixed_output:
		output_container = fixed_output
	else:
		output_container = "user.%nickname%.swup.{config}.%in:name[1]%.%in:name[2]%.{sample}.{suffix}".format(
				config=opts.config,
				sample=opts.sample,
				suffix=opts.grid_suffix,
			)

	driver.options().setString("nc_outputSampleName", output_container)
	if opts.gb_per_job and opts.gb_per_job > 0:
		driver.options().setDouble("nc_nGBPerJob", opts.gb_per_job)

	driver.options().setDouble(EL.Job.optGridMergeOutput, 1)

	_check_proxy()
	driver.submitOnly(job, opts.output)

	opts_file = open(os.path.join(opts.output, "swup-opts"), "w")
	pickle.dump(opts, opts_file)
	opts_file.close()

def parse_options(mode=None):
	import argparse
	import truth_lite.ext.argcomplete as argcomplete
	import truth_lite.ArgTools as AT

	def_files_per_job = int(os.getenv("SWUP_DEFAULT_FILES_PER_JOB", "10"))

	parser = argparse.ArgumentParser()
	parser.add_argument("--dry-run", "-d", action="store_true", help="no actual action.")
	parser.add_argument("--verbose", "-v", action="store_true", help="enable verbose output.")	
	parser.add_argument("--config", "-c", choices=AT.get_configs(), required=True, help="The configuration to use")
	parser.add_argument("--output", "-o", help="The output directory, default: output/<config>/<sample>")
	parser.add_argument("--nevents", "-n", type=int, help="Maximal number of events to process")
	parser.add_argument("--systematic", "-s", help="Systematic variation to use")

	parser.add_argument("-f", action="append", dest="exec_lines", help="Additional lines to execute somehwere in the configuration. ONLY USE THIS FOR VERY QUICK TESTS!")

	if not mode or mode == Mode.batch:
		parser.add_argument("--queue", "-q", default="1nh", help="Batch queue, only for submit.py (default=1nh)")	
		parser.add_argument("--batch", choices=BT.BATCH_SYSTEMS, help="Select batch system (default: auto-detect)")
		parser.add_argument("--files-per-job", type=int, default=def_files_per_job, help="Number of files per worker, larger means less jobs but longer processing times per job (default=%(default)d)")
	
	if not mode or mode == Mode.grid:
		parser.add_argument("--grid-suffix", "-g", default="AUTO", help="Grid output suffix (default=AUTO)")
		parser.add_argument("--gb-per-job", type=float, default=10, help="Controls the --nGBPerJob option, use negative values to remove default of 10GB (default=10)")

	if not mode or mode != Mode.grid:
		parser.add_argument("sample", choices=AT.get_samples(), help="The sample to use")
	else:
		parser.add_argument("dataset", help="The dataset to use")
		parser.add_argument("sample", help="The sample name")

	argcomplete.autocomplete(parser)
	opts = parser.parse_args()
	opts.workDir = os.getenv("WorkDir")

	if opts.dry_run:
		print "doing a dry run, nothing will be executed."
		

	if not mode or mode != Mode.grid:
		opts.sample_path = ST.input_path(opts.sample, validate=True)

	opts.config_path = os.path.join(opts.workDir, "truth_lite/share/config", opts.config + ".py")
	if not os.path.exists(opts.config_path):
		print "Error: Failed to find config '%s' in '%s'" % (opts.config, opts.config_path)
		sys.exit(1)

	if not opts.output:
		opts.output = ST.output_path(opts.sample, opts.config, syst=opts.systematic, create_base=True)

	return opts
