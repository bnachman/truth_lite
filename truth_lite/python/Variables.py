from collections import namedtuple

_V = namedtuple("Variable", "title unit binning tags")

PHI_BINNING = "20,-3.2,3.2"
ETA_BINNING = "20,-2.5,2.5"

Variables = {
	'n_jet': _V("Jet multiplicity", None, "15,-.5,14.5", tags=["common"]),
	'n_bjet': _V("b-jet multiplicity", None, "5,-.5,4.5", tags=["common"]),
	'n_fatjet': _V("large-R jet multiplicity", None, "9,-.5,8.5", tags=[]),

	'met': _V("E_{T}^{miss}", "GeV", "45,100,1000", tags=["common"]),
	'met_cst': _V("E_{T}^{miss} (CST)", "GeV", "45,100,1000", tags=["common"]),
	'met_sumet': _V("sumet", "GeV", "100,0,4000", tags=["common"]),
	'mt': _V("m_{T}", "GeV", "30,0,600", tags=["common"]),
	'cst_mt': _V("m_{T} (CST)", "GeV", "30,0,600", tags=["common"]),
	'neutrino_met': _V("truth HS E_{T}^{miss}", "GeV", "45,100,1000", tags=["common"]),

	'amt2': _V("am_{T2}", "GeV", "25,0,500", tags=["common"]),
	'mt2_tau': _V("m_{T2}^{#tau}", "GeV", "25,0,500", tags=["common"]),
	'topness': _V("topness", None, "30,-10,20", tags=["common"]),
	'ht': _V("H_{T}", "GeV", "20,0,400", tags=["common"]),
	'ht_sig': _V("H_{T}^{miss} significance", None, "20,0,20", tags=["common"]),
	'met_sig': _V("E_{T}^{miss}/#sqrt{H_{T}}", None, "50,0,50", tags=["common"]),
	'm_top': _V("#DeltaR based m_{t,had}", "GeV", "30,100,400", tags=["common"]),
	'm_top_chi2': _V("#chi^{2} based m_{t,had}", "GeV", "25,100,350", tags=["common"]),

	'lep_pt[0]': _V("lepton p_{T}", "GeV", "50,0,500", tags=["common"]),
	'lep_phi[0]': _V("lepton #phi", None, PHI_BINNING, tags=["common"]),
	'lep_eta[0]': _V("lepton #eta", None, ETA_BINNING, tags=["common"]),

	'jet_pt[0]': _V("first jet p_{T}", "GeV", "50,0,500", tags=["common"]),
	'jet_phi[0]': _V("first jet #phi", None, PHI_BINNING, tags=["common"]),
	'jet_eta[0]': _V("first jet #eta", None, ETA_BINNING, tags=["common"]),

	'jet_pt[1]': _V("second jet p_{T}", "GeV", "35,0,350", tags=["common"]),
	'jet_pt[2]': _V("third jet p_{T}", "GeV", "25,0,250", tags=["common"]),
	'jet_pt[3]': _V("fourth jet p_{T}", "GeV", "20,0,200", tags=["common"]),

	'bjet_pt[0]': _V("first b-jet p_{T}", "GeV", "50,0,500", tags=["common"]),
	'bjet_phi[0]': _V("first b-jet #phi", None, PHI_BINNING, tags=["common"]),
	'bjet_eta[0]': _V("first b-jet #eta", None, ETA_BINNING, tags=["common"]),

	'fatjet_pt[0]': _V("first large-R jet p_{T}", "GeV", "65,150,800", tags=[]),
	'fatjet_phi[0]': _V("first large-R jet #phi", None, PHI_BINNING, tags=[]),
	'fatjet_eta[0]': _V("first large-R jet #eta", None, ETA_BINNING, tags=[]),
	'fatjet_m[0]': _V("first large-R jet mass", "GeV", "70,0,350", tags=[]),
	'fatjet_width[0]': _V("first large-R jet width", None, "50,0,1", tags=[]),
	'fatjet_split12[0]': _V("first large-R jet splitting scale", "GeV", "50,0,300", tags=[]),
	'fatjet_tau1[0]': _V("first large-R jet #tau_{1}", None, "50,0,1", tags=[]),
	'fatjet_tau2[0]': _V("first large-R jet #tau_{2}", None, "50,0,1", tags=[]),
	'fatjet_tau3[0]': _V("first large-R jet #tau_{3}", None, "50,0,1", tags=[]),
	'fatjet_tau21[0]': _V("first large-R jet #tau_{2}/#tau_{1}", None, "50,0,1", tags=[]),
	'fatjet_tau31[0]': _V("first large-R jet #tau_{3}/#tau_{1}", None, "50,0,1", tags=[]),
	'fatjet_tau32[0]': _V("first large-R jet #tau_{3}/#tau_{2}", None, "50,0,1", tags=[]),

	'minDRfatjetTop': _V("min #DeltaR(truth top, J_{1})", None, "50,0,2", tags=[]),

	'truth_met_reso': _V("(E_{T,reco}^{miss} - E_{T,truth}^{miss}) / E_{T,truth}^{miss}", None, "100,-1.2,1.2", tags=["truth"]),
	'truth_met_reso_phi': _V("(#phi(E_{T,reco}^{miss}) - #phi(E_{T,truth}^{miss})) / #phi(E_{T,truth}^{miss})", None, "100,-1.2,1.2", tags=["truth"]),

	'truth_met': _V("truth E_{T}^{miss}", "GeV", "50,0,1000", tags=["truth"]),
	'truth_met_sumet': _V("truth sumet", "GeV", "100,0,1000", tags=["truth"]),

	'met-truth_met': _V("E_{T,reco}^{miss} - E_{T,truth}^{miss}", "GeV", "100,-50,50", tags=["truth"]),
	'met_phi-truth_met_phi': _V("#phi(E_{T,reco}^{miss}) - #phi(E_{T,truth}^{miss})", "rad", "100,-3.2,3.2", tags=["truth"]),


	'met_perp': _V("perp. E_{T}^{miss}", "GeV", "80,0,800", tags=["top"]),
	'ttbar_m': _V("m_{t#bar{t}}", "GeV", "50,0,1500", tags=["top"]),
	'ttbar_pt': _V("p_{T}(t#bar{t})", "GeV", "50,0,500", tags=["top"]),

	'abs(dphi_ttbar)': _V("|#Delta#phi(t_{lep},t_{had})|", None, "50,0,3.2", tags=["top"]),
	'abs(dphi_leptop_met)': _V("|#Delta#phi(t_{lep},E_{T}^{miss})|", None, "50,0,3.2", tags=["top"]),
	'abs(dphi_hadtop_met)': _V("|#Delta#phi(t_{had},E_{T}^{miss})|", None, "50,0,3.2", tags=["top"]),

	'truth_met_perp': _V("truth perp. E_{T}^{miss}", "GeV", "80,0,800", tags=["top"]),
	'truth_ttbar_m': _V("truth m_{t#bar{t}}", "GeV", "50,0,1500", tags=["top"]),
	'truth_ttbar_pt': _V("truth p_{T}(t#bar{t})", "GeV", "50,0,500", tags=["top"]),

	'abs(truth_dphi_ttbar)': _V("truth |#Delta#phi(t_{lep},t_{had})|", None, "50,0,3.2", tags=["top"]),
	'abs(truth_dphi_leptop_met)': _V("truth |#Delta#phi(t_{lep},E_{T}^{miss})|", None, "50,0,3.2", tags=["top"]),
	'abs(truth_dphi_hadtop_met)': _V("truth |#Delta#phi(t_{had},E_{T}^{miss})|", None, "50,0,3.2", tags=["top"]),


	'incb_met_perp': _V("perp. E_{T}^{miss}", "GeV", "80,0,800", tags=["top"]),
	'incb_ttbar_m': _V("m_{t#bar{t}}", "GeV", "50,0,1500", tags=["top"]),
	'incb_ttbar_pt': _V("p_{T}(t#bar{t})", "GeV", "50,0,500", tags=["top"]),
	'abs(incb_dphi_ttbar)': _V("|#Delta#phi(t_{lep},t_{had})|", None, "50,0,3.2", tags=["top"]),
	'abs(incb_dphi_leptop_met)': _V("|#Delta#phi(t_{lep},E_{T}^{miss})|", None, "50,0,3.2", tags=["top"]),
	'abs(incb_dphi_hadtop_met)': _V("|#Delta#phi(t_{had},E_{T}^{miss})|", None, "50,0,3.2", tags=["top"]),


	'tt_cat': _V("t#bar{t} decay", None, "11,0,11", tags=["common"]),
	'TT_decay_mode': _V("TT decay", None, "8,0.5,8.5", tags=[]),
}

BinLabels = {
	'tt_cat': ("#font[12]{l}#font[12]{l}", "#font[12]{l}h", "#font[12]{l}#tau_{h}", "#font[12]{l}#tau_{#font[12]{l}}",
	           "#tau_{#font[12]{l}}h", "#tau_{#font[12]{l}}#tau_{h}", "#tau_{#font[12]{l}}#tau_{#font[12]{l}}", "hh",
	           "h#tau_{h}", "#tau_{h}#tau_{h}", "other" ),

	'TT_decay_mode': (
		"bWbW", "tZtZ", "bWtZ", "tHtH", "bWtH", "tZtH", "", "Error",
	),
}

ConvertToGeV = [
	'met', 'mt','ht', 'cst_mt', 'neutrino_met',
	'jet_pt', 'lep_pt','bjet_pt',
	'm_top',
	'fatjet_pt',
	'fatjet_m',
	'met-truth_met',

	'met_perp', 'ttbar_m', 'ttbar_pt',
	'truth_met',
	'truth_met_perp', 'truth_ttbar_m', 'truth_ttbar_pt',
	'incb_met_perp', 'incb_ttbar_m', 'incb_ttbar_pt',
]

ConversionBlacklist = [
	'mt2_tau', 'm_top_chi2', 'ht_sig', 'met_sig',
	'met_phi', 'truth_met_phi',

	'truth_met_reso',
	'truth_met_reso_phi',
]

ConversionBlacklistMarkers = [
	'*', '/', ':',
]

def title(var):
	return Variables.get(var, _V(var, None, None, [])).title

def unit(var):
	return Variables.get(var, _V(var, None, None, [])).unit

def binning(var, uservalue=None):
	if uservalue:
		return uservalue

	if ":" in var:
		yname, xname = var.split(":")

		yvar = Variables.get(yname, None)
		xvar = Variables.get(xname, None)

		if yvar and xvar and yvar.binning and yvar.binning:
			return "%s,%s" % (xvar.binning, yvar.binning)
		return None

	return Variables.get(var, _V(var, None, uservalue, [])).binning

def conv_unit(var):
	if var in ConversionBlacklist:
		return var

	for marker in  ConversionBlacklistMarkers:
		if marker in var:
			return var

	for conv in ConvertToGeV:
		if var.startswith(conv):
			return "((%s)*1e-3)" % var

	return var
