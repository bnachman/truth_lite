
# register for autocomplete
for f in truth_lite/scripts/*.py; do 
	if grep 'ARGCOMPLETE' $f &> /dev/null; then 
		bf=`basename $f`
		eval "$(truth_lite/python/ext/argcomplete/register-python-argcomplete $bf)"
	fi
done
