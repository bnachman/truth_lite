
if [ "$1" = "" ]; then
	echo "Please source this script as 'source truth_lite/scripts/batch_setup.sh $WorkDir'"
	exit 1
fi

echo "Batch setup for truth_lite"

export WorkDir=$1
export CALIBPATH=$WorkDir/truth_lite/share/:$HOME/AtlasCalib:$ROOTCOREBIN/download:$ROOTCOREBIN/data:/cvmfs/atlas.cern.ch/repo/sw/database/GroupData:/afs/cern.ch/atlas/www/GROUPS/DATABASE/GroupData:http//atlas.web.cern.ch/Atlas/GROUPS/DATABASE/GroupData:$CALIBPATH

echo "Environment:"
env

echo "Batch setup done"
