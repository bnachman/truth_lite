#!/usr/bin/env python
import sys
import os
import pprint

_orig_argv = sys.argv[:]
sys.argv = [_orig_argv[0]]

import ROOT
ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 1001;")
from ROOT import *

sys.argv = _orig_argv

try:
	import pyAMI.client
	import pyAMI.atlas.api as atlas_api
except:
	print "Please call"
	print "  localSetupPyAMI"
	print "  voms-proxy-init -voms atlas"
	print "before using this script"
	sys.exit(1)

import truth_lite.SampleTools as Smp
import truth_lite.ext.termcolor as tc

def check_sample_stats(ami_client, sample):
	name = sample.name()
	name = name.replace("mc15_13TeV:", "").replace("DAOD_SUSY5", "AOD")
	if name[-5] == "p":
		name = name[:-6]

	print name

	stat_hist = sample.readHist("DerivationStat_Events")
	cbk_events = int(stat_hist.GetBinContent(1))

	ami_info = atlas_api.get_dataset_info(ami_client, name)
	if len(ami_info) != 1:
		print "WARNING: %d results returned from AMI, expected 1" % len(ami_info)

	ami_events = int(ami_info[0]["totalEvents"])
	color = 'green' if cbk_events == ami_events else "red"
	rel_diff = float(ami_events) / float(cbk_events) - 1

	tc.cprint("CBK: %9d, AMI: %9d -> rel. diff: %+7.2f%%" % (cbk_events, ami_events, round(rel_diff*100,2)), color)

	return ami_events == cbk_events, ami_events

def parse_opts():
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("--config", "-c", required=True, help="The configuration of the sample")
	parser.add_argument("sample", help="The sample")

	opts = parser.parse_args()
	opts.sample_id = Smp.parse_sample_id(opts, opts.sample)
	opts.sample_path = Smp.output_path(opts.sample_id.name, opts.sample_id.config, opts.sample_id.syst, validate=True)
	opts.sample = Smp.load_output_sample_from_id(opts.sample_id)

	return opts

def main():
	opts = parse_opts()

	print "Initializing PyAMI"
	ami_client = pyAMI.client.Client("atlas")
	atlas_api.init()

	ami_events_db = {}

	print "Checking %d sub-samples" % opts.sample.hist.size()
	for sample in opts.sample.hist:
		is_ok, ami_events = check_sample_stats(ami_client, sample)

		if not is_ok and ami_events > 0:
			chid = Smp.get_mc_id(sample)
			ami_events_db[chid] = ami_events

	if len(ami_events_db) != 0:
		pycode = pprint.pformat(ami_events_db)
		print "Please update truth_lite/python/AmiEvents.py with:"
		tc.cprint("# %s" % opts.sample.name, "yellow")
		tc.cprint("AMI_EVENTS.update(%s)" % pycode, "yellow")

if __name__ == '__main__':
	main()
