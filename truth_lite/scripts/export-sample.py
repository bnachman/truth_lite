#!/usr/bin/env python
import sys
import os
import shutil
from array import array

_orig_argv = sys.argv[:]
sys.argv = [_orig_argv[0]]

import ROOT
ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
from ROOT import *

sys.argv = _orig_argv


import truth_lite.SampleTools as Smp

def _write_string(rootfile, name, string):
	obj = TObjString(string)
	rootfile.WriteTObject(obj, name)

def write_metadata(opts, hists, target):
	weight = Smp.sample_weight(hists)
	weight_param = TParameter(float)("Weight", weight)
	target.WriteTObject(weight_param)

	_write_string(target, "Id", Smp.sample_id(opts.sample))
	_write_string(target, "Name", opts.sample.name)
	_write_string(target, "Config", opts.sample.config)
	_write_string(target, "Syst", opts.sample.syst or "Nominal")

def copy_cutflows(opts, hists, target):
	histfilename = hists.fileName(0)
	histfile = TFile.Open(histfilename)

	histnames = []
	for key in histfile.GetListOfKeys():
		name = key.GetName()
		if name.startswith("passed"):
			histnames.append(name)

	histfile.Close()

	for name in histnames:
		hist = hists.readHist(name)
		target.WriteTObject(hist, name)

def nice_title(dataset):
	if ":" in dataset:
		dataset = dataset.split(":")[1]

	if dataset.startswith("user."):
		p2 = dataset.find(".", len("user."))
		dataset = dataset[p2+1:]

	if ".e" in dataset:
		tags = dataset.find(".e")
		dataset = dataset[:tags]

	for part in (".merge", ".DxAOD", ".AOD", ".SUSY5"):
		dataset = dataset.replace(part, "")

	return dataset

def export_subsample(opts, data, hist):
	title = nice_title(data.name())

	outfile_name = os.path.join(opts.output_path, title + ".root")

	if os.path.exists(outfile_name):
		if opts.force:
			os.remove(outfile_name)
		else:
			print "Target file", outfile_name, "already exists, skipping..."
			return

	root_files = map(lambda s: s.replace("file://", ""), [data.fileName(i) for i in xrange(data.numFiles())])

	hadd_cmd = "hadd -v 0 {target} {sources}".format(target=outfile_name, sources=" ".join(root_files))
	os.system(hadd_cmd)

	target = TFile(outfile_name, "update")
	tree = target.Get("CollectionTree")

	xs_var = array('f', [Smp.sample_weight(hist)])
	xs_branch = tree.Branch("xs_weight", xs_var, "xs_weight/F")

	num_entries = tree.GetEntries()
	for i in xrange(num_entries):
		tree.GetEntry()
		xs_branch.Fill()

	target.WriteTObject(tree)

	write_metadata(opts, hist, target)
	copy_cutflows(opts, hist, target)

	target.Close()

def parse_options():
	import argparse

	default_output = os.path.join(os.getenv("WorkDir"), "export")

	parser = argparse.ArgumentParser()
	parser.add_argument("-f", "--force", action="store_true", help="Remove existing output")
	parser.add_argument("-o", "--output", default=default_output, help="Target directory (default: %(default)s)")
	parser.add_argument("-c", "--config", help="The configuration")
	parser.add_argument("--syst", help="Select a systematic")
	parser.add_argument("-t", "--tree", default="ntuple", help="The sample output tree (default: %(default)s)")
	parser.add_argument("sample", help="The sample id, in the form [config[#syst]:]sample")

	opts = parser.parse_args()

	opts.sample_id = Smp.parse_sample_id(opts, opts.sample)
	opts.sample_path = Smp.output_path(opts.sample_id.name, opts.sample_id.config, opts.sample_id.syst, validate=True)
	opts.sample = Smp.load_output_sample_from_id(opts.sample_id, data_type=opts.tree)

	opts.output_path = os.path.join(opts.output, opts.sample.config)
	if opts.sample.syst:
		opts.output_path = os.path.join(opts.output_path, opts.sample.syst)
	opts.output_path = os.path.join(opts.output_path, opts.sample.name)

	if not os.path.exists(opts.output_path):
		os.makedirs(opts.output_path)

	return opts

def main():
	opts = parse_options()

	for i in xrange(opts.sample.data.size()):
		smp_data = opts.sample.data.at(i)
		smp_hist = opts.sample.hist.at(i)

		sys.stdout.write("Exporting " + smp_data.name())
		sys.stdout.flush()
		export_subsample(opts, smp_data, smp_hist)
		sys.stdout.write("\tdone!\n")

if __name__ == '__main__':
	main()
