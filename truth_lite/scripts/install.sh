SAVE_MODE=true
if [ "$1" = "-y" ]; then
	SAVE_MODE=false
fi

source truth_lite/share/truth_liteConfig.sh

if [ ! "$ROOTCOREBIN" = "" ]; then
	echo "RootCore already setup, going to un-setup..."
	rcSetup -u

	echo "Deleting old extra packages"
	if $SAVE_MODE; then
		echo "Going to ask for each package, use 'source .../install.sh -y' in order to disable this check"
	fi
	for dir in `find -maxdepth 1 -type d -and -not -name truth_lite`; do
		if [ -e $dir/.svn -a -e $dir/cmt -a ! -e $dir/.keep ]; then
			echo $dir

			if $SAVE_MODE; then
				rm -rfI $dir/
			else
				rm -rf $dir/
			fi
		fi
	done
fi

source truth_lite/scripts/setup.sh new

rc find_packages
rc clean
rc compile

mkdir -p samples
mkdir -p output
mkdir -p plots

cp truth_lite/share/post-merge.hook .git/hooks/post-merge
chmod +x .git/hooks/post-merge

if [ ! -L "setup.sh" ]; then
	ln -s truth_lite/scripts/setup.sh .
fi
