#!/usr/bin/env python
import sys
import os

if '_ARGCOMPLETE' not in os.environ:
	_orig_argv = sys.argv[:]
	sys.argv = [_orig_argv[0]]

	import ROOT
	ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
	from ROOT import *

	sys.argv = _orig_argv

from truth_lite.RunTools import *

def main():
	opts = parse_options()

	load_config(opts)

	sh = load_sample(opts)
	job = create_job(opts, sh)

	run_job(opts, job)

if __name__ == '__main__':
	main()
