#!/usr/bin/env python
import sys
import os

_orig_argv = sys.argv[:]
sys.argv = [_orig_argv[0]]

import ROOT
ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
from ROOT import *

sys.argv = _orig_argv

import truth_lite.SampleTools as Smp

def parse_options():
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("--full-path", "-f", action="store_true", help="Full path to sample is provided")
	parser.add_argument("--type", "-t", choices=("str", "float"), default="str", help="Meta type")
	parser.add_argument("sample", help="The sample name")
	parser.add_argument("key", help="Meta key")
	parser.add_argument("value", help="The meta value")

	opts = parser.parse_args()
	if not opts.full_path:
		opts.sample_path = Smp.input_path(opts.sample, validate=True)
	else:
		opts.sample_path = opts.sample

	return opts

def main():
	opts = parse_options()

	print "Loading sample", opts.sample
	sh = SH.SampleHandler()
	sh.load(opts.sample_path)

	print "Setting meta data: %s = %s" % (opts.key, opts.value)
	if opts.type == "str":
		sh.setMetaString(opts.key, opts.value)
	elif opts.type == "float":
		sh.setMetaDouble(opts.key, float(opts.value))

	print "Saving sample"
	sh.save(opts.sample_path)

if __name__ == '__main__':
	main()
