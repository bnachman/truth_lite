#!/usr/bin/env python
"""
setup-common-samples.py

setup all or some of the samples that are defined in $WorkDir/truth_lite/share/common-samples.
"""
import sys
import os


def parse_options():
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("-d", "--datasets",  default='*.txt', help="The file(s) defining the sample names, can use '*' to match many  (default is '*.txt')")
	parser.add_argument("-v", "--verbose",  action="store_true", help="verbose output")
        
	opts = parser.parse_args()

	return opts

def main():
	opts = parse_options()

        workingDir = os.getenv('WorkDir', './')
        from glob import glob
        list_of_dataset_files = glob('%s/truth_lite/share/common-samples/%s' % (workingDir,opts.datasets) )
        if opts.verbose: print 'going to process the following',len(list_of_dataset_files),' file(s):',list_of_dataset_files

        for df in list_of_dataset_files:
            afile = open(df,'r')
            for line in afile.xreadlines():
                line=line.strip()
                #print line
                dq2_exp = line
                sampleName = (line.split(':')[-1]).strip('/')
                cmd = "setup-sample.py --dq %s %s" % (dq2_exp, sampleName)
                if opts.verbose: print cmd
                os.system( cmd )
            afile.close()

        

if __name__ == '__main__':
	main()

