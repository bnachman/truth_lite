#!/usr/bin/env python
"""
setup-sample.py

Use this script to create or reset input samples. To create a sample, a list of
files is used that is supplied in a filelist, a text file containing one entry
per line. Metadata can be given in comments
"""
import sys
import os
from collections import namedtuple

import truth_lite.LumiDB as lumidb

if '_ARGCOMPLETE' not in os.environ:
	_orig_argv = sys.argv[:]
	sys.argv = [_orig_argv[0]]

	import ROOT
	ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
	from ROOT import *

	sys.argv = _orig_argv

import truth_lite.SampleTools as Smp
import truth_lite.BatchTools as BT

def conv_color(string):
	val = gROOT.ProcessLine("%s;" % string)
	return float(val)

MD = namedtuple("MetaData", "type default")
CommonMetaData = {
	'xs': MD('float', None),
	'gen_events': MD('float', None),
	'color': MD('color', None),
	'title': MD('str', lambda opts: opts.sample),
	'title_root': MD('str', None),
}

TypeConv = {
	'str': str,
	'float': float,
	'color': conv_color,
}

SusyToolsDatabases = [
	'mc12_8TeV', 'mc14_13TeV',
]


def get_dq2_datasets(file_or_ds):
	if not os.path.exists(file_or_ds):
		return [file_or_ds]

	res = []

	ds_list = open(file_or_ds, "r")
	for line in ds_list:
		line = line.strip()
		if line.startswith("#") or line == "":
			continue

		res.append(line)
	ds_list.close()

	return res

def read_metadata(filename):
	infile = open(filename, "r")

	meta = {}

	for line in infile:
		# only consider "comments" in the form
		# "# field: value" or
		# "# type field: value"
		if not line.startswith("#") or not ":" in line:
			continue

		type_name, value = line.split(":", 1)

		if type_name.startswith("#"):
			type_name = type_name[1:].strip()

		if " " in type_name:
			ftype, name = type_name.split(" ", 1)
			ftype = ftype.strip()
			name = name.strip()
		else:
			ftype = "str"
			name = type_name
			if name in CommonMetaData:
				ftype = CommonMetaData[name].type

		if ftype not in TypeConv.keys():
			print "Warning: Meta field with unknown type '%s'" % ftype
			print "         Line: '%s'" % line
			continue

		meta[name] = TypeConv[ftype](value.strip())

	infile.close()

	return meta

def create_sample_from_file(opts, sh, ds):
	meta = read_metadata(opts.file)

	if 'dataset' not in meta and not opts.dataset:
		print "Error: No dataset specified"
		sys.exit(1)

	dataset = opts.dataset if opts.dataset else meta['dataset']

	if dataset.endswith("/"):
		dataset = dataset[:-1]

	SH.readFileList(sh, dataset, opts.file)

	return sh.at(sh.size()-1), meta

def create_sample(opts, sh, ds):
	if os.path.exists(ds):
		return create_sample_from_file(opts, sh, ds)

	if not ds.endswith("/"):
		ds += "/"

	SH.scanDQ2(sh, ds)
	return sh.at(sh.size()-1), {}

def read_meta(opts, meta, field, default, ftype=None):
	prompt = "{field} [{default}]: ".format(field=field, default=default)
	inp = raw_input(prompt)

	if inp == "":
		meta[field] = default
	else:
		if not ftype:
			if field in CommonMetaData:
				ftype = CommonMetaData[field].type
			else:
				ftype = "str"

		value = TypeConv[ftype](inp)
		meta[field] = value

def ask_meta_data(opts, meta):
	print "Please enter the common meta data fields"
	for field, md in CommonMetaData.iteritems():
		if not field in meta:
			default = md.default
			if type(default) == type(lambda a:a):
				default = default(opts)
			read_meta(opts, meta, field, default, md.type)

	print "Additional meta data"
	while True:
		field = raw_input("Field name [blank to finish]")
		if field == "":
			break
		read_meta(opts, meta, field, "")

def set_lumi_info(opts, sh):
	lumidb.init()

	total_lumi = 0
	bad_runs = []
	for sample in sh:
		name = sample.name()
		parts = name.split(".")
		p0 = 0
		for i, part in enumerate(parts):
			if part.startswith("data15"):
				p0 = i
				break
		run = parts[p0+1]
		if run.startswith("00"):
			run = run[2:]

		lumi = lumidb.get_run_lumi(run)
		if lumi == -1:
			bad_runs.append(sample)
			continue

		print "Luminosity for run %s: %.2f/pb" % (run, lumi)
		total_lumi += lumi

		sample.setMetaDouble("run", int(run))
		sample.setMetaDouble("lumi", lumi)

	print "Total luminosity: %.2f/pb" % total_lumi

	if opts.rm_unknown:
		for smp in bad_runs:
			sh.remove(smp)

		print "Removed %d runs without luminosity information, %d runs remaining." % (len(bad_runs), sh.size())

def init_sample(opts, sh, meta={}):
	if opts.susy_tools and not opts.data:
		path = gSystem.ExpandPathName("$ROOTCOREBIN/data/SUSYTools/" + opts.susy_tools)
		if os.path.isfile(path):
			SH.readSusyMeta(sh, path)
		elif os.path.isdir(path):
			SH.readSusyMetaDir(sh, path)
		else:
			print "Error: Failed to find '%s'" % path
			print "       Will not read susy meta data..."

		for smp in sh:
			xs = smp.getMetaDouble("nc_xs", 0.)
			kfactor = smp.getMetaDouble("kfactor", 0.)
			filtereff = smp.getMetaDouble("filter_efficiency", 0.)

			eff_xs = xs * kfactor * filtereff
			smp.setMetaDouble("xs", eff_xs)

			if eff_xs == 0:
				print "Warning: Failed to find cross section information for", smp.name()
	elif opts.data:
		set_lumi_info(opts, sh)

	if opts.interactive:
		ask_meta_data(opts, meta)

	if opts.data:
		meta['data'] = 1.0

	for key, val in meta.iteritems():
		print "Set meta: {field}={val}".format(field=key, val=val)

		if type(val) == str:
			sh.setMetaString(key, val)
		elif type(val) == float:
			sh.setMetaDouble(key, val)
		elif val == None:
			continue
		else:
			assert(False)

	if not opts.disable_scan:
		print "Scanning event content"
		SH.scanNEvents(sh)


def save_sample(opts, sh):
	path = Smp.input_path(opts.sample, validate=False)
	sh.save(path)

def parse_options():
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("--data", action="store_true", help="Indicate as data")
	parser.add_argument("--dataset", "-n", help="The dataset name of the sample")
	parser.add_argument("--reset", "-r", action="store_true", help="Do not create a new sample, reset the existing one")
	parser.add_argument("--file", "-f", type=argparse.FileType('r'), help="The dataset list file")
	parser.add_argument("--susy-tools", default="mc15_13TeV", help="Use susy tools cross sections database")
	parser.add_argument("--interactive", "-i", action="store_true", help="Interactively ask for some meta data")
	parser.add_argument("--dq2", help="Add dq2 datasets (provide either a dataset name or a file with a list of datasets)")
	parser.add_argument("--batch", choices=BT.BATCH_SYSTEMS, default=BT.detect(silent=True), help="The local batch system")
	parser.add_argument("--allow-partial", action="store_true", help="Allow partial samples found over DQ2")
	parser.add_argument("--disable-scan", action="store_true", help="Do not scan for number of events per file")
	parser.add_argument("--rm-unknown", action="store_true", help="Remove data runs without luminosity information")
	parser.add_argument("sample", help="The sample name")

	opts = parser.parse_args()

	if not opts.reset:
		if not opts.file and not opts.dq2:
			print "Error: Expecting input file to create a sample"

		if opts.file:
			opts.file = opts.file.name

	return opts

def check_sample_type(opts, datasets):
	# auto determine whether sample(s) are data or MC
	isData = len([ds for ds in datasets if 'physics_Main' in ds]) == len(datasets)
	isMC   = len([ds for ds in datasets if ('mc15_' in ds) or ('mc14_' in ds) or ('mc13_' in ds) or ('mc12_' in ds)]) == len(datasets)
	if isData and not isMC:
		print 'auto checking type of samples, all datasets appear to be data. Setting to data.'
		opts.data = True
	elif not isData and isMC:
		print 'auto checking type of samples, all datasets appear to be MC. Setting data to False.'
		opts.data = False
	else:
		print 'auto checking type of samples, no clear indication. Set by hand using the --data option.'

def main():
	opts = parse_options()

	if not opts.reset:
		datasets = []

		if opts.dq2:
			datasets = get_dq2_datasets(opts.dq2)
		else:
			datasets = [opts.file]

		check_sample_type(opts, datasets)

		sh = SH.SampleHandler()

		for ds in datasets:
			smp, meta = create_sample(opts, sh, ds)
		
		if opts.dq2:
			disk = BT.GROUPDISK[opts.batch]
			repl = BT.LOCAL_PATH[opts.batch]

			SH.makeGridDirect(sh, disk, repl[0], repl[1], opts.allow_partial)
	else:
		sh = Smp.load(Smp.input_path(opts.sample, validate=True))

	init_sample(opts, sh)
	save_sample(opts, sh)

if __name__ == '__main__':
	main()

