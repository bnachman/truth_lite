#include <iostream>
#include <TH1F.h>

TH1F *g_SFhist = 0;

double ReweightSF(double rwValue)
{
	if (!g_SFhist) {
		std::cerr << "g_SFhist not set\n";
		return 0.;
	}

	const double sf = g_SFhist->GetBinContent(g_SFhist->FindBin(rwValue));
	return sf;
}
