#include <iostream>

#define ONLY_VLQ_ENUM
#include <truth_lite/ClassifyTT.h>

double ReweightVLQ_ZWH(bool isTTSignal, int decayMode, double Zbr, double Wbr, double Hbr)
{
	static const double Zbr_orig = 1./3.;
	static const double Wbr_orig = 1./3.;
	static const double Hbr_orig = 1./3.;

	if (!isTTSignal)
		return 1.0;

	switch (decayMode) {
	case VLQ_BWBW:
		return Wbr*Wbr / (Wbr_orig*Wbr_orig);
	case VLQ_BWTZ:
		return Wbr*Zbr / (Wbr_orig*Zbr_orig);
	case VLQ_BWTH:
		return Wbr*Hbr / (Wbr_orig*Hbr_orig);
	case VLQ_TZTZ:
		return Zbr*Zbr / (Zbr_orig*Zbr_orig);
	case VLQ_TZTH:
		return Zbr*Hbr / (Zbr_orig*Hbr_orig);
	case VLQ_THTH:
		return Hbr*Hbr / (Hbr_orig*Hbr_orig);
	}

	if (decayMode & VLQ_ERROR) {
		return 0;
	}

	std::cout << "Warning: Unknown decay mode " << decayMode << " -- removing event\n";
	return 0;
}
