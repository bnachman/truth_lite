
###############################################################################
#bVeto

def cut_bVetoNEW_base(commonData, cutflow, weight):
	# define pT cuts for 4 leading jets
	jet_pt_cuts = [80*GeV]

	#check jet pT cuts
	for i, ptcut in enumerate(jet_pt_cuts):
		if commonData.signalJets.at(i).pt() < ptcut:
			return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal

        #dphi(met,jets)
	jet1 = commonData.signalJets.at(0)
	if _abs_delta_phi(jet1, met) < 0.6:
		return 0

	jet2 = commonData.signalJets.at(1)
	if _abs_delta_phi(jet2, met) < 0.6:
		return 0

	cutflow.passCut(weight)

	return weight

add_virtual_cut("bVetoNEW_base", cut_bVetoNEW_base, [
	"1 jet > 80",
	"DeltaPhi(j12,met) > 0.6",
])


###############
# bveto SR

def cut_bVetoNEW_SR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("bVetoNEW_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if not commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	#met
	met = commonData.metRefFinal
	if met.met() < 200*GeV:
		return 0
	cutflow.passCut(weight)

	#jet pT - no

	# met sig
	if commonData.eventInfo.auxdata(float)("METSig") < 12:
		return 0
	cutflow.passCut(weight)

	#topness
	if commonData.eventInfo.auxdata(float)("Topness") < 2:
		return 0
	cutflow.passCut(weight)

	# amt2
	if commonData.eventInfo.auxdata(float)("aMT2_GeV") < 150:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("mT") < 130*GeV :
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("bVetoNEW_SR", cut_bVetoNEW_SR, [
	"bVetoNEW_base",
	"== 0 bjet",
	"MET > 200",
	"METSig > 12",
	"topness > 2",
	"amT2 > 150",
	"mT > 130",
])

add_virtual_cut_ex("bVetoNEW_SR_ex", [
	Cut("j1_pt", "jet1 pT > 80", lambda data: data.signalJets.size() >= 1 and data.signalJets.at(0).pt >= 80*GeV),
	Cut("0b", "==0 bjet", lambda data: data.bJets.size() == 0),
	Cut("dphi_j1_met", "dPhi(j1,met) > 0.6", lambda data: data.signalJets.size() >= 1 and _abs_delta_phi(data.signalJets.at(0), data.metRefFinal) > 0.6),
	Cut("dphi_j2_met", "dPhi(j2,met) > 0.6", lambda data: data.signalJets.size() >= 2 and _abs_delta_phi(data.signalJets.at(1), data.metRefFinal) > 0.6),

	Cut("met", "MET > 200", lambda data: data.metRefFinal.met() >= 200*GeV),
	Cut("metsig", "METSig > 12", lambda data: data.eventInfo.auxdata(float)("METSig") >= 12),
	Cut("mt", "mT > 130 GeV", lambda data: data.eventInfo.auxdata(float)("mT") >= 130*GeV),
	Cut("topness", "topness > 2", lambda data: data.eventInfo.auxdata(float)("Topness") >= 2),
	Cut("amt2", "amT2 >= 150", lambda data: data.eventInfo.auxdata(float)("aMT2_GeV") >= 150),
], save_sub_results=True)
