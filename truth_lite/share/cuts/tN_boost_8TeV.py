###############################################################################
# tN_boost

def cut_tN_boost_base(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("presel_4j") <= 0:
		return 0
	cutflow.passCut(weight)

	jet_pt_cuts = [75*GeV, 65*GeV, 40*GeV, 25*GeV]
	for i, ptcut in enumerate(jet_pt_cuts):
		if commonData.signalJets.at(i).pt() < ptcut:
			return 0
	cutflow.passCut(weight)

	if not commonData.signalFatJets or commonData.signalFatJets.empty():
		return 0

	J1 = commonData.signalFatJets.at(0)
	if J1.pt() < 270*GeV or J1.m() < 75*GeV:
		return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal
	if commonData.signalFatJets.size() > 1:
		J2 = commonData.signalFatJets.at(1)

		if J2.pt() > 150*GeV and abs(J2.eta()) < 2:
			if _abs_delta_phi(J2, met) < 0.85:
				return 0
	cutflow.passCut(weight)

	if not commonData.bJets.empty() and commonData.bJets.at(0).p4().DeltaR(_get_lep(commonData).p4()) > 2.6:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("HTSig") < 10:
		return 0
	cutflow.passCut(weight)

	jet1 = commonData.signalJets.at(0)
	jet2 = commonData.signalJets.at(1)

	if _abs_delta_phi(jet1, met) < 0.5:
		return 0
	if _abs_delta_phi(jet2, met) < 0.3:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("tN_boost_base", cut_tN_boost_base, [
	"presel_4j",
	"4 jets > 75,65,40,25",
	">= 1 fatjet (pT > 270, m > 75)",
	"DeltaPhi(fajet2,met) > 0.85",
	"DeltaR(b,l) < 2.6",
	"HTSig > 10",
	"DeltaPhi(j12,met) > 0.5,0.3",
])


###############
# tN_boost SR

def cut_tN_boost_SR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("tN_boost_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal
	if met.met() < 315*GeV:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("mT") < 175*GeV:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("aMT2_GeV") < 145:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("Topness") < 7:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("tN_boost_SR", cut_tN_boost_SR, [
	"tN_boost_base",
	">= 1 bjet",
	"MET > 315",
	"mT > 175",
	"amT2 > 145",
	"topness > 7",
])


###############
# tN_boost CR

def cut_tN_boost_TCR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("tN_boost_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal
	if met.met() < 260*GeV:
		return 0
	cutflow.passCut(weight)

	mT = commonData.eventInfo.auxdata(float)("mT")
	if mT < 60*GeV or mT > 90*GeV:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("aMT2_GeV") < 130:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("tN_boost_TCR", cut_tN_boost_TCR, [
	"tN_boost_base",
	">= 1 bjet",
	"MET > 260",
	"mT in [60,90]",
	"amT2 > 130",
])



def cut_tN_boost_WCR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("tN_boost_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if not commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal
	if met.met() < 260*GeV:
		return 0
	cutflow.passCut(weight)

	mT = commonData.eventInfo.auxdata(float)("mT")
	if mT < 60*GeV or mT > 90*GeV:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("aMT2_GeV") < 130:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("tN_boost_WCR", cut_tN_boost_WCR, [
	"tN_boost_base",
	"== 0 bjet",
	"MET > 260",
	"mT in [60,90]",
	"amT2 > 130",
])


###############
# tN_boost VR

def cut_tN_boost_TVR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("tN_boost_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal
	if met.met() < 260*GeV:
		return 0
	cutflow.passCut(weight)

	mT = commonData.eventInfo.auxdata(float)("mT")
	if mT < 90*GeV or mT > 120*GeV:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("aMT2_GeV") < 130:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("tN_boost_TVR", cut_tN_boost_TVR, [
	"tN_boost_base",
	">= 1 bjet",
	"MET > 260",
	"mT in [90,120]",
	"amT2 > 130",
])



def cut_tN_boost_WVR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("tN_boost_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if not commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal
	if met.met() < 260*GeV:
		return 0
	cutflow.passCut(weight)

	mT = commonData.eventInfo.auxdata(float)("mT")
	if mT < 90*GeV or mT > 120*GeV:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("aMT2_GeV") < 130:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("tN_boost_WVR", cut_tN_boost_WVR, [
	"tN_boost_base",
	"== 0 bjet",
	"MET > 260",
	"mT in [90,120]",
	"amT2 > 130",
])

