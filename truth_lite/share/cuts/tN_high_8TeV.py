###############################################################################
# tN_high

add_virtual_cut_ex("tN_high_SR", [
	Cut("4j", "4 jets > 25", lambda data: data.signalJets.size() >= 4),
	Cut("j1_pt", "jet1 pT > 100", lambda data: ((data.signalJets.size() >= 1) and (data.signalJets.at(0).pt() >= 100*GeV))),
	Cut("j2_pt", "jet2 pT >  80", lambda data: ((data.signalJets.size() >= 2) and (data.signalJets.at(1).pt() >=  80*GeV))),
	Cut("j3_pt", "jet3 pT >  40", lambda data: ((data.signalJets.size() >= 3) and (data.signalJets.at(2).pt() >=  40*GeV))),
	Cut("1b", ">=1 bjet", lambda data: data.bJets.size() >= 1),
	Cut("dR_bjet_lep", "dR(lep,b-jet) <= 3", lambda data: (data.bJets.size() >= 1 and data.bJets.at(0).p4().DeltaR(_get_lep(data).p4()) <= 3)),
	Cut("met", "MET > 320", lambda data: data.metRefFinal.met() >= 320*GeV),
	Cut("htsig", "HTSig > 12.5", lambda data: data.eventInfo.auxdata(float)("HTSig") >= 12.5),
	Cut("mt", "mT > 200 GeV", lambda data: data.eventInfo.auxdata(float)("mT") >= 200*GeV),
	Cut("mtop", "m(t_had) in [130,250]", lambda data: (data.eventInfo.auxdata(float)("TopMassChi2") >= 130 and data.eventInfo.auxdata(float)("TopMassChi2") <= 250)), #this is in GeV!
	Cut("amt2", "amT2 >= 170", lambda data: data.eventInfo.auxdata(float)("aMT2_GeV") >= 170),
	Cut("mt2tau", "mt2tau >= 120", lambda data: data.eventInfo.auxdata(float)("mT2tau_GeV") >= 120),
], save_sub_results=True)


add_virtual_cut_ex("tN_high_TCR", [
	Cut("4j", "4 jets > 25", lambda data: data.signalJets.size() >= 4),
	Cut("j1_pt", "jet1 pT > 100", lambda data: ((data.signalJets.size() >= 1) and (data.signalJets.at(0).pt() >= 100*GeV))),
	Cut("j2_pt", "jet2 pT >  80", lambda data: ((data.signalJets.size() >= 2) and (data.signalJets.at(1).pt() >=  80*GeV))),
	Cut("j3_pt", "jet3 pT >  40", lambda data: ((data.signalJets.size() >= 3) and (data.signalJets.at(2).pt() >=  40*GeV))),
	Cut("1b", ">=1 bjet", lambda data: data.bJets.size() >= 1),
	Cut("dR_bjet_lep", "dR(lep,b-jet) <= 3", lambda data: (data.bJets.size() >= 1 and data.bJets.at(0).p4().DeltaR(_get_lep(data).p4()) <= 3)),
	Cut("met", "MET > 225", lambda data: data.metRefFinal.met() >= 225*GeV),
	Cut("htsig", "HTSig > 8.8", lambda data: data.eventInfo.auxdata(float)("HTSig") >= 8.8),
	Cut("mt", "mT in [60,90] GeV", lambda data: data.eventInfo.auxdata(float)("mT") >= 60*GeV and data.eventInfo.auxdata(float)("mT") <= 90*GeV),
	Cut("mtop", "m(t_had) in [130,250]", lambda data: (data.eventInfo.auxdata(float)("TopMassChi2") >= 130 and data.eventInfo.auxdata(float)("TopMassChi2") <= 250)), #this is in GeV!
])

add_virtual_cut_ex("tN_high_WCR", [
	Cut("4j", "4 jets > 25", lambda data: data.signalJets.size() >= 4),
	Cut("j1_pt", "jet1 pT > 100", lambda data: ((data.signalJets.size() >= 1) and (data.signalJets.at(0).pt() >= 100*GeV))),
	Cut("j2_pt", "jet2 pT >  80", lambda data: ((data.signalJets.size() >= 2) and (data.signalJets.at(1).pt() >=  80*GeV))),
	Cut("j3_pt", "jet3 pT >  40", lambda data: ((data.signalJets.size() >= 3) and (data.signalJets.at(2).pt() >=  40*GeV))),
	Cut("0b", "== 0 bjet", lambda data: data.bJets.size() == 0),
	Cut("met", "MET > 225", lambda data: data.metRefFinal.met() >= 225*GeV),
	Cut("htsig", "HTSig > 8.8", lambda data: data.eventInfo.auxdata(float)("HTSig") >= 8.8),
	Cut("mt", "mT in [60,90] GeV", lambda data: data.eventInfo.auxdata(float)("mT") >= 60*GeV and data.eventInfo.auxdata(float)("mT") <= 90*GeV),
	Cut("mtop", "m(t_had) in [130,250]", lambda data: (data.eventInfo.auxdata(float)("TopMassChi2") >= 130 and data.eventInfo.auxdata(float)("TopMassChi2") <= 250)), #this is in GeV!
])


add_virtual_cut_ex("tN_high_TVR", [
	Cut("4j", "4 jets > 25", lambda data: data.signalJets.size() >= 4),
	Cut("j1_pt", "jet1 pT > 100", lambda data: ((data.signalJets.size() >= 1) and (data.signalJets.at(0).pt() >= 100*GeV))),
	Cut("j2_pt", "jet2 pT >  80", lambda data: ((data.signalJets.size() >= 2) and (data.signalJets.at(1).pt() >=  80*GeV))),
	Cut("j3_pt", "jet3 pT >  40", lambda data: ((data.signalJets.size() >= 3) and (data.signalJets.at(2).pt() >=  40*GeV))),
	Cut("1b", ">=1 bjet", lambda data: data.bJets.size() >= 1),
	Cut("dR_bjet_lep", "dR(lep,b-jet) <= 3", lambda data: (data.bJets.size() >= 1 and data.bJets.at(0).p4().DeltaR(_get_lep(data).p4()) <= 3)),
	Cut("met", "MET > 225", lambda data: data.metRefFinal.met() >= 225*GeV),
	Cut("htsig", "HTSig > 8.8", lambda data: data.eventInfo.auxdata(float)("HTSig") >= 8.8),
	Cut("mt", "mT in [90,120] GeV", lambda data: data.eventInfo.auxdata(float)("mT") >= 90*GeV and data.eventInfo.auxdata(float)("mT") <= 120*GeV),
	Cut("mtop", "m(t_had) in [130,250]", lambda data: (data.eventInfo.auxdata(float)("TopMassChi2") >= 130 and data.eventInfo.auxdata(float)("TopMassChi2") <= 250)), #this is in GeV!
])

add_virtual_cut_ex("tN_high_WVR", [
	Cut("4j", "4 jets > 25", lambda data: data.signalJets.size() >= 4),
	Cut("j1_pt", "jet1 pT > 100", lambda data: ((data.signalJets.size() >= 1) and (data.signalJets.at(0).pt() >= 100*GeV))),
	Cut("j2_pt", "jet2 pT >  80", lambda data: ((data.signalJets.size() >= 2) and (data.signalJets.at(1).pt() >=  80*GeV))),
	Cut("j3_pt", "jet3 pT >  40", lambda data: ((data.signalJets.size() >= 3) and (data.signalJets.at(2).pt() >=  40*GeV))),
	Cut("0b", "== 0 bjet", lambda data: data.bJets.size() == 0),
	Cut("met", "MET > 225", lambda data: data.metRefFinal.met() >= 225*GeV),
	Cut("htsig", "HTSig > 8.8", lambda data: data.eventInfo.auxdata(float)("HTSig") >= 8.8),
	Cut("mt", "mT in [90,120] GeV", lambda data: data.eventInfo.auxdata(float)("mT") >= 90*GeV and data.eventInfo.auxdata(float)("mT") <= 120*GeV),
	Cut("mtop", "m(t_had) in [130,250]", lambda data: (data.eventInfo.auxdata(float)("TopMassChi2") >= 130 and data.eventInfo.auxdata(float)("TopMassChi2") <= 250)), #this is in GeV!
])

