
add_virtual_cut_ex("tN_high_opt_SR", [
	Cut("4j", "4 jets > 25", lambda data: data.signalJets.size() >= 4),
	Cut("j1_pt", "jet1 pT > 100", lambda data: ((data.signalJets.size() >= 1) and (data.signalJets.at(0).pt() >= 100*GeV))),
	Cut("j2_pt", "jet2 pT >  80", lambda data: ((data.signalJets.size() >= 2) and (data.signalJets.at(1).pt() >=  80*GeV))),
	Cut("j3_pt", "jet3 pT >  40", lambda data: ((data.signalJets.size() >= 3) and (data.signalJets.at(2).pt() >=  40*GeV))),
	Cut("1b", ">=1 bjet", lambda data: data.bJets.size() >= 1),
	Cut("dR_bjet_lep", "dR(lep,b-jet) <= 3", lambda data: (data.bJets.size() >= 1 and data.bJets.at(0).p4().DeltaR(_get_lep(data).p4()) <= 3)),
	Cut("met", "MET > 380", lambda data: data.metRefFinal.met() >= 380*GeV),
	Cut("htsig", "HTSig > 12.5", lambda data: data.eventInfo.auxdata(float)("HTSig") >= 12.5),
	Cut("mt", "mT > 240 GeV", lambda data: data.eventInfo.auxdata(float)("mT") >= 240*GeV),
	Cut("mtop", "m(t_had) in [130,250]", lambda data: (data.eventInfo.auxdata(float)("TopMassChi2") >= 130 and data.eventInfo.auxdata(float)("TopMassChi2") <= 250)), #this is in GeV!
	Cut("amt2", "amT2 >= 170", lambda data: data.eventInfo.auxdata(float)("aMT2_GeV") >= 170),
	Cut("mt2tau", "mt2tau >= 120", lambda data: data.eventInfo.auxdata(float)("mT2tau_GeV") >= 120),
], save_sub_results=False)
