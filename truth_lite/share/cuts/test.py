
def test_cut(commonData, cutflow, weight):
	mT = commonData.eventInfo.auxdata(float)("mT")
	if mT <= 50*GeV:
		return 0
	cutflow.passCut(weight)

	nJets = commonData.signalJets.size()
	if nJets < 4:
		return 0
	cutflow.passCut(weight)
	return weight

add_virtual_cut("test", test_cut, ['mT > 50 GeV', 'nJets >= 5'])

add_virtual_cut_ex("test_ex", [
	Cut("mT", "mT > 50GeV", lambda data: data.eventInfo.auxdata(float)("mT") > 50*GeV),
	Cut("nJets", "nJets >= 4", lambda data: data.signalJets.size() >= 4),
	])