# This is an example configuration file for the plot.py script

PlotConfig = {
	'stack': [
		S('ttbar1', 't#bar{t}', TColor.GetColor("#5E9AD6")),
		S('wjets', 'scaled W+jets', TColor.GetColor("#FCDD5D"), scale_factor=(0.9, 0.2)), # scale W+jets by 0.9+/-0.2
		S('singletop', 'Single top', TColor.GetColor("#82DE68")),
		S('diboson', 'Diboson', TColor.GetColor("#54C571")),
		S('zjets', 'Z+jets', TColor.GetColor("#FFA04D")),
		S('ttV', 't#bar{t}+V', TColor.GetColor("#E67067")),
	],
	'overlay': [
		S('stop_800_1', 'm(#tilde{t},#tilde{#chi}_{1}^{0})=(800,1) GeV', kRed+1),
	],

	# This entry is optional. If present, a list of systematics is expedted which are
	# evaluated and shown in the uncertainty band. The entries should not contain the
	# "__1up" or "__1down" part, plot.py handles this automatically (i.e. one entry per 
	# systematic, whether its double or single sided)
	'systematics': [
		"JET_GroupedNP_1",
		"JET_GroupedNP_2",
		"JET_GroupedNP_3",
		"JET_JER_SINGLE_NP",
		"PRW_DATASF",
	],
	
	# Either use a sample, or 'pseudodata', to take the total SM as data points. This
	# can be used to test, how the plot would look like including data (this enables
	# the Data/SM ratio and some different decorations). If  no data is included,
	# the string "simulation" will be displayed under the ATLAS string.
	'data': None,
	#'data': "pseudodata",

	# Set to anything not-false to enable the ATLAS text in the top-left corner. If set
	# to a string (i.e. "Internal", "Preliminary", ...), the string will be printed
	# after the ATLAS, if set to True only ATLAS is written.
	'atlas': "Work in progress",

	# The auto-scale option controls whether overlays (i.e. signal points) are scaled
	# to be visible. If the maximum bin of the signal distribution is less than 10% (1%)
	# of the maximum of the combined SM distribution for linear (log) plots, than it is
	# scaled by a "nice" (i.e. rounded) factor to reach ~ the 10% (1%).
	'auto-scale': True,

	# If the auto-others option is enabled, small SM samples (< 5% of total SM for the
	# integral) are grouped together to an "Others" entry. This happens only for linear
	# y-axis plots.
	'auto-others': True,
}
