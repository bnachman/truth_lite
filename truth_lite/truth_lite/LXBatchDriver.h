#ifndef ntupSoftwareSU_LXBatch_DRIVER_HH
#define ntupSoftwareSU_LXBatch_DRIVER_HH


#include <EventLoop/Global.h>

#include <EventLoop/BatchDriver.h>
#include <SampleHandler/Global.h>

namespace EL
{
  class LXBatchDriver : public BatchDriver
  {
    //
    // public interface
    //

    /// effects: test the invariant of this object
    /// guarantee: no-fail
  public:
    void testInvariant () const;


    /// effects: standard default constructor
    /// guarantee: strong
    /// failures: low level errors I
  public:
    LXBatchDriver ();



    //
    // interface inherited from BatchDriver
    //

    /// effects: perform the actual torque submission with njob jobs
    /// guarantee: strong
    /// failures: submission errors
    /// rationale: the virtual part of batch submission
  private:
    virtual void
    batchSubmit (const std::string& location, const SH::MetaObject& options,
		 std::size_t njob) const;



    //
    // private interface
    //

    ClassDef(LXBatchDriver, 1);
  };
}

#endif