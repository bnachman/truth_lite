#ifndef truth_lite_NtupleOutput_H
#define truth_lite_NtupleOutput_H

#include <array>
#include <unordered_map>

#include <EventLoop/Algorithm.h>
#include <EventLoopAlgs/NTupleSvc.h>

#include <TTree.h>

namespace truth_lite {

class CommonData;

namespace Output {
  class Particle;
  class Met;
}

class NtupleOutput : public EL::Algorithm
{
  CommonData* commonData; //!
  EL::NTupleSvc *ntupleSvc; //!

public:
  std::string outputName = "ntuple";

  // this is a standard constructor
  NtupleOutput ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(NtupleOutput, 1);

  struct ContainerOutput
  {
    static const size_t MAX_LEN = 50;
    int length;
    std::array<float, MAX_LEN> pt;
    std::array<float, MAX_LEN> eta;
    std::array<float, MAX_LEN> phi;
    std::array<float, MAX_LEN> e;
    std::unordered_map<std::string, std::array<double, MAX_LEN>> decorationsD;
    std::unordered_map<std::string, std::array<float,  MAX_LEN>> decorationsF;
    std::unordered_map<std::string, std::array<long,   MAX_LEN>> decorationsL;
    std::unordered_map<std::string, std::array<int,    MAX_LEN>> decorationsI;
    std::unordered_map<std::string, std::array<char,   MAX_LEN>> decorationsC;
  };

  struct MetOutput
  {
    std::string container;
    std::string name;

    float met;
    float met_x;
    float met_y;
    float met_phi;
    float met_sumet;
  };

private:
  // actually these are not stored as doubles, but have to be retrieved as such
  std::vector<std::pair<std::string, float>> dynamicDoubles; //!
  std::vector<std::pair<std::string, float>> dynamicFloats; //!
  std::vector<std::pair<std::string, int>> dynamicInts; //!
  std::vector<std::pair<std::string, int>> dynamicUInts; //!
  std::vector<std::pair<std::string, int>> dynamicULongs; //!

  void initializeTree(TTree *tree);

  std::map<std::string, ContainerOutput> containers;
  std::map<std::string, MetOutput> metOutputs;

  void AddParticleArray(TTree *tree, const Output::Particle& def);
  void FillParticleArray(const std::string& containerName);

  void AddMet(TTree *tree, const Output::Met& def);
  void FillMet(MetOutput& out);

  template <typename T>
  void AddDynamicBranches(TTree *tree, const std::vector<std::pair<std::string, std::string>>& names, std::vector<std::pair<std::string, T>>& storage, const char *typeDef);

  void AddBranch(TTree *tree, const std::string& name, void *ptr, const std::string& type, const std::string& desc = "");
  bool BranchFilterAccept(const std::string& name);
};

}

#endif
