#ifndef truth_lite_Setup_Truth_Objects_H
#define truth_lite_Setup_Truth_Objects_H

#include <EventLoop/Algorithm.h>

namespace truth_lite {

class CommonData;

class Setup_Truth_Objects : public EL::Algorithm
{
  CommonData* commonData; //!

public:

  // this is a standard constructor
  Setup_Truth_Objects ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(Setup_Truth_Objects, 1);
};

}

#endif
